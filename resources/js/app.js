import Vue from 'vue'
import store from '~/Store'
import router from '~/Router'
import i18n from '~/Plugins/i18n'
import App from '~/Components/App'

import '~/Plugins'
import '~/Components'
// store.state.lang.locale
Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
