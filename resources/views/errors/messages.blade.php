@if (Session::has('success'))
<div class="alert alert-success alert-dismissible m-3">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-success"></i> Success!</h5>
    {{Session::get('success')}}
</div>
@elseif (Session::has('error'))
<div class="alert alert-danger alert-dismissible m-3">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-danger"></i> Error!</h5>
    {{Session::get('error')}}
</div>
@elseif (Session::has('msg'))
<div class="alert alert-info alert-dismissible m-3">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-info"></i> Warning!</h5>
    {{Session::get('msg')}}
</div>
{{Session::forget('validateError')}}
@elseif (Session::has('validateError'))
<div class="alert alert-info alert-dismissible m-3">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-info"></i> Warning!</h5>
    {{Session::get('validateError')}}
</div>
{{Session::forget('validateError')}}
@elseif (Session::has('message'))
<div class="alert alert-success alert-dismissible m-3">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-success"></i> Success!</h5>
    {{Session::get('message')}}
</div>
@endif

<div class="alert alert-success alert-dismissible m-3" id="success_msg" style="display: none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-success"></i> Success!</h5>
    <p id="success_msg_id"></p>
</div>

<div class="alert alert-danger alert-dismissible m-3" id="error_msg" style="display: none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-danger"></i> Error!</h5>
    <p id="error_msg_id"></p>
</div>