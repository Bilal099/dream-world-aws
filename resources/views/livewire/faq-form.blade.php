<div>
    {{--  action="{{route('faq.store')}}" --}}
    {{-- <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        @if (session()->has('validateError'))
            <div class="alert alert-danger">
                {{ session('validateError') }}
            </div>
        @endif
    </div> --}}
    <form id="quickForm" wire:submit.prevent="{{$_id?"update":"save"}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card card-primary mt-4">
            <div class="card-header">
                <h3 class="card-title">Add Faq</h3>
            </div>
            @include('errors.messages')
            <div class="card-body ">
                @php
                    $i = 1;
                @endphp
                @if (count($languages) > 1)
                    <ul class="nav nav-tabs nav-justified">
                        @foreach ($languages as $item)
                            <li class="nav-item @error('question.'.$item->code) danger @enderror @error('answer.'.$item->code) danger @enderror">
                                <a wire:click="changeTabs('{{$item->code}}')" class="nav-link {{($tabActive==$item->code)? 'active':'' }}" data-toggle="tab"
                                    href="#tab{{$item->code}}">{{@$item->language}}</a>
                            </li>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    </ul>
                @endif
                <div class="tab-content">
                    @foreach ($languages as $item)
                        <div id="tab{{$item->code}}" class="tab-pane {{($tabActive==$item->code)? 'active':'' }} ">
                            <div class="form-group col-md-12"  wire:key="question-{{$item->code}}">
                                <label for="question">Question {{ count($languages) > 1? @$item->language: "" }}</label>
                                <input type="text" id="question" class="form-control" wire:model="question.{{$item->code}}" />
                                @error('question.'.$item->code)
                                    <p class="text-danger text-sm">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="form-group col-md-12"  wire:key="answer-{{$item->code}}">
                                <label for="answer">Answer {{ count($languages) > 1? @$item->language: "" }}</label>
                                <input type="text" id="answer" class="form-control" wire:model="answer.{{$item->code}}" />
                                @error('answer.'.$item->code)
                                    <p class="text-danger text-sm">{{$message}}</p>
                                @enderror
                            </div>
                        </div>
                    @endforeach
                    
                    <div class="form-group col-md-12">
                        <div class="form-check ">
                            <input type="checkbox" class="form-check-input" id="active" wire:model="active">
                            Is Active?
                            @error('active')
                                <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="submit_btn">Save</button>
                <a href="{{route('faq.index')}}" class="btn btn-default">Back</a>
            </div>
        </div>
    </form>
</div>
