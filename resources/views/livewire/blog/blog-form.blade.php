<div>
    
    <form id="quickForm" wire:submit.prevent="{{$_id?"update":"save"}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card card-primary mt-4">
            <div class="card-header">
                <h3 class="card-title">Add Blog</h3>
            </div>
            @include('errors.messages')
            <div class="card-body ">
                @php
                    $i = 1;
                @endphp
                @if (count($languages) > 1)
                    <ul class="nav nav-tabs nav-justified">
                        @foreach ($languages as $item)
                            <li class="nav-item @error('title.'.$item->code) danger @enderror @error('description.'.$item->code) danger @enderror">
                                <a wire:click="changeTabs('{{$item->code}}')" class="nav-link {{($tabActive==$item->code)? 'active':'' }}" data-toggle="tab"
                                    href="#tab{{$item->code}}">{{@$item->language}}</a>
                            </li>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    </ul>
                @endif
                <div class="tab-content">
                    @foreach ($languages as $item)
                        <div id="tab{{$item->code}}" class="tab-pane {{($tabActive==$item->code)? 'active':'' }} ">
                            <div class="form-group col-md-12"  wire:key="title-{{$item->code}}">
                                <label for="title">Title {{ count($languages) > 1? @$item->language: "" }}</label>
                                <input type="text" id="title" class="form-control" wire:model.debounce.200ms="title.{{$item->code}}" />
                                @error('title.'.$item->code)
                                    <p class="text-danger text-sm">{{$message}}</p>
                                @enderror
                            </div>

                            <div class="form-group col-md-12" wire:ignore  wire:key="description-{{$item->code}}">
                                <label for="description">Description {{ count($languages) > 1? @$item->language: "" }}</label>
                                <textarea name="" data-desc="@this" id="description_{{$item->code}}" class="form-control description-ck" cols="30" rows="10" wire:model.defer="description.{{$item->code}}"></textarea>
                            </div>
                            @error('description.'.$item->code)
                                <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                        </div>
                    @endforeach
                        @if(count($languages) > 1) 
                        <hr>
                        @endif
                    <div class="form-group col-md-12">
                        <label for="answer">Image</label>
                        <input type="file" class="form-control-file" wire:model.defer="image">
                        @error('image')
                            <p class="text-danger text-sm">{{$message}}</p>
                        @enderror
                    </div>

                    @if ($image)
                        <div class="form-group col-md-12">
                            <label for="answer">Image Preview:</label>
                            <br>
                            <img src="{{ $image->temporaryUrl() }}" style="width: 400px;height:auto;">
                        </div>
                        
                    @endif

                    @if ($imageName != null)
                        <div class="form-group col-md-12">
                            <label for="answer">Uploaded Image:</label>
                            <br>
                            <img src="{{asset('/storage/'.$imageName)}}" alt="" style="width: 400px;height:auto;">

                        </div>
                    @endif
                    
                    <div class="form-group col-md-12">
                        <div class="form-check ">
                            <input type="checkbox" class="form-check-input" id="active" wire:model="active">
                            Is Active?
                            @error('active')
                                <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="submit_btn">Save</button>
                <a href="{{route('blog.index')}}" class="btn btn-default">Back</a>
            </div>
        </div>
    </form>
    <script src="{{asset('public/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    
@push('custom-script')
<script>
    class MyUploadAdapter {
        constructor( loader ) {
            // The file loader instance to use during the upload. It sounds scary but do not
            // worry — the loader will be passed into the adapter later on in this guide.
            this.loader = loader;
        }

        // Starts the upload process.
        upload() {
            return this.loader.file
                .then( file => new Promise( ( resolve, reject ) => {
                    this._initRequest();
                    this._initListeners( resolve, reject, file );
                    this._sendRequest( file );
                } ) );
        }

        // Aborts the upload process.
        abort() {
            if ( this.xhr ) {
                this.xhr.abort();
            }
        }
        // Initializes the XMLHttpRequest object using the URL passed to the constructor.
        _initRequest() {
            const xhr = this.xhr = new XMLHttpRequest();

            // Note that your request may look different. It is up to you and your editor
            // integration to choose the right communication channel. This example uses
            // a POST request with JSON as a data structure but your configuration
            // could be different.
            xhr.open( 'POST', '{{route("ckeditor.imageUpload")}}', true );
            xhr.setRequestHeader("x-csrf-token","{{csrf_token()}}")
            xhr.responseType = 'json';
        }
        // Initializes XMLHttpRequest listeners.
        _initListeners( resolve, reject, file ) {
            const xhr = this.xhr;
            const loader = this.loader;
            const genericErrorText = `Couldn't upload file: ${ file.name }.`;

            xhr.addEventListener( 'error', () => reject( genericErrorText ) );
            xhr.addEventListener( 'abort', () => reject() );
            xhr.addEventListener( 'load', () => {
                const response = xhr.response;

                // This example assumes the XHR server's "response" object will come with
                // an "error" which has its own "message" that can be passed to reject()
                // in the upload promise.
                //
                // Your integration may handle upload errors in a different way so make sure
                // it is done properly. The reject() function must be called when the upload fails.
                if ( !response || response.error ) {
                    return reject( response && response.error ? response.error.message : genericErrorText );
                }

                // If the upload is successful, resolve the upload promise with an object containing
                // at least the "default" URL, pointing to the image on the server.
                // This URL will be used to display the image in the content. Learn more in the
                // UploadAdapter#upload documentation.
                resolve( {
                    default: response.url
                } );
            } );

            // Upload progress when it is supported. The file loader has the #uploadTotal and #uploaded
            // properties which are used e.g. to display the upload progress bar in the editor
            // user interface.
            if ( xhr.upload ) {
                xhr.upload.addEventListener( 'progress', evt => {
                    if ( evt.lengthComputable ) {
                        loader.uploadTotal = evt.total;
                        loader.uploaded = evt.loaded;
                    }
                } );
            }
        }
        // Prepares the data and sends the request.
        _sendRequest( file ) {
            // Prepare the form data.
            const data = new FormData();

            data.append( 'upload', file );

            // Important note: This is the right place to implement security mechanisms
            // like authentication and CSRF protection. For instance, you can use
            // XMLHttpRequest.setRequestHeader() to set the request headers containing
            // the CSRF token generated earlier by your application.

            // Send the request.
            this.xhr.send( data );
        }
    }
    function MyCustomUploadAdapterPlugin( editor ) {
        editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
            // Configure the URL to the upload script in your back-end here!
            return new MyUploadAdapter( loader );
        };
    }

    $(document).ready(function () {
        getLanguage();        
    });

    // ClassicEditor
    //     .create( document.querySelector( '#description' ), {
    //         extraPlugins: [ MyCustomUploadAdapterPlugin ],
    //     } )
    //     .then( editor => {
    //         // window.editor = editor;
    //         @if ($_id)
    //         editor.setData('{!!$description["en"]!!}')
    //         @endif
    //         editor.model.document.on('change:data',()=>{
    //             let desc = $('#description').data('desc');
    //             eval(desc).set('description.en',editor.getData())
    //         })
            
    //     } )
    //     .catch( err => {
    //         console.error( err.stack );
    //     } );


        function getLanguage()
        {
            let obj;
            let code = Array();
            let i = 0;
            $.ajax({
                type: "GET",
                url: "{{route('AjaxCallToGetLanguages')}}",
                success: function (response) {
                    obj = response['languages'];
                    $.each(obj, function (indexInArray, valueOfElement) { 
                        code[i] = valueOfElement['code'];
                        i++;
                    });
                    setEditor(code);
                }
            });
        }

        function setEditor(code) 
        {
            code.forEach(element => {
                ClassicEditor
                .create( document.querySelector( '#description_'+element ), {
                    extraPlugins: [ MyCustomUploadAdapterPlugin ],
                } )
                .then( editor => {
                    @if ($_id)
                    @endif
                    editor.model.document.on('change:data',()=>{
                        @if (!session()->has('success'))
                            let desc = $('#description_'+element).data('desc');
                            eval(desc).set('description.'+element,editor.getData())
                        @endif
                    })
                } )
                .catch( err => {
                    console.error( err.stack );
                } );
            });
            
        }
</script>
@endpush
</div>
