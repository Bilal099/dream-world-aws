<div>
    <div class="card mt-3">
        <div class="card-header">
          <h3 class="card-title white-text">View All Blogs</h3>
        <a class="btn btn-success btn-sm float-right" href="{{route('blog.create')}}"><i class="fa fa-plus"></i></a>
        
        </div>
        <!-- /.card-header -->
        @include('errors.messages')
        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Title</th>
                        {{-- <th>Description</th> --}}
                        
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key=> $item)
                        <tr>
                        <td>{{++$key}}</td>
                            <td>{{$item->title}}</td>
                            {{-- <td>{!!$item->description !!}</td> --}}
                            <td>{{$item->active == 1?'active':'deactive'}}</td>
                            <td>
                                @can('blog-edit')
                                    <a href="{{route("blog.edit",$item->id)}}" title="permissions" class="btn btn-sm btn-secondary"><i class="fa fa-edit"></i></a>
                                    
                                @endcan

                                @can('blog-delete')
                                    @if($confirming===$item->id)
                                        <button wire:click="delete({{ $item->id }})"
                                            class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="right" title="Are you sure?"><i class="fa fa-trash"></i></button>
                                    @else
                                        <button wire:click="confirmDelete({{ $item->id }})"
                                            class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="right" title="Do you want to Delete this?"><i class="fas fa-times"></i></button>
                                    @endif
                                @endcan

                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>
