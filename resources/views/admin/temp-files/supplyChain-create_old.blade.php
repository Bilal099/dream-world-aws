@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Supply Chain</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form id="supply_chain_form" action="{{route('supplyChain.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                        <select name="deparment_id" id="department" class="form-control">
                                            <option disabled selected>Select Department</option>
                                            @foreach ($department as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                        <h4 class="caption-subject bold uppercase"> <a class="btn btn-success" style="float: right;" id="add_detail">Add More Detail</a></h4>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <table class="table table-striped table-bordered table-responsive" style="margin-top:20px ">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>CHQ Name</th>
                                                    <th>NTN/CNIC Number</th>
                                                    <th>GL IT CODE  (A/Cs)</th>
                                                    <th>Nature Of Work</th>
                                                    <th>Purchaser Person </th>
                                                    <th>Total Amount</th>
                                                    <th>Category</th>
                                                    {{-- <th style="">Mode of Payment</th> --}}
                                                    {{-- <th>Approved Amount </th>
                                                    <th>Approved by</th> --}}
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {{-- @if (old('date') != null)
                                                @foreach (old('date') as $key => $oldValues)
                                                
                                                <tr>
                                                    <td>
                                                        <div class="form-group @error('date.'.$key) has-error @enderror">
                                                            <input type="date" name="date[]" class="" value="{{old('date.'.$key)}}" placeholder="" id="" readonly>
                                                        </div>
                                                        @error('date.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    
                                                    <td>
                                                        <div class="form-group @error('chq_name.'.$key) has-error @enderror">
                                                            <input type="text" name="chq_name[]" class="" value="{{old('chq_name.'.$key)}}" placeholder="" id="">
                                                        </div>
                                                        @error('chq_name.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('cnic.'.$key) has-error @enderror">
                                                            <input maxlength="13" type="text" name="cnic[]" class="nic" value="{{old('cnic.'.$key)}}" placeholder="" id="">
                                                        </div>
                                                        @error('cnic.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('gt_item_code.'.$key) has-error @enderror">
                                                            <input type="text" name="gt_item_code[]" class="" value="{{old('gt_item_code.'.$key)}}" placeholder="" id="">
                                                        </div>
                                                        @error('gt_item_code.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('nature_of_work.'.$key) has-error @enderror">
                                                            <input type="text" name="nature_of_work[]" class="" value="{{old('nature_of_work.'.$key)}}" placeholder="" id="">
                                                        </div>
                                                        @error('nature_of_work.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('purchaser_person.'.$key) has-error @enderror">
                                                            <input type="text" name="purchaser_person[]" class="" value="{{old('purchaser_person.'.$key)}}" placeholder="" id="">
                                                        </div>
                                                        @error('purchaser_person.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('total_amount.'.$key) has-error @enderror">
                                                            <input type="text" name="total_amount[]" class="" value="{{old('total_amount.'.$key)}}" placeholder="" id="">
                                                        </div>
                                                        @error('total_amount.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('category.'.$key) has-error @enderror">
                                                            <select name="category[{{@$item->id}}]" id="category"  class="">
                                                                <option selected disabled>Select Option</option>
                                                                @foreach ($category as $obj)
                                                                    <option value="{{$obj->id}}" {{old('category.'.$key)==$obj->id? 'selected':''}}>{{$obj->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        @error('category.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        @if ($key != 0)
                                                            <button type="button" class="btn btn-danger deleteRow"><i class="fa fa-trash"></i></button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach --}}
                                                {{-- @else --}}
                                                <tr>
                                                    <td>
                                                        <input type="date" name="date[]" class="form-control supply-chain-date" placeholder="" id="supply_chain_date" readonly>
                                                    </td>
                                                    <td>
                                                        {{-- <input type="text" name="chq_name[]" class="" value="{{old('chq_name')}}" placeholder="" id=""> --}}
                                                        <select class="vendor-name required" name="chq_name[]" id="" required>
                                                            {{-- <option disabled selected>Select Vendor</option> --}}
                                                            <option value="" selected>Select Vendor</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input maxlength="13" type="text" name="cnic[]" class="nic" value="" placeholder="" id="" required>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="gt_item_code[]" class="gt_item_code" value="" placeholder="" id="" required>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="nature_of_work[]" class="nature_of_work" value="" placeholder="" id="" required>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="purchaser_person[]" class="purchaser_person" value="" placeholder="" id="" required>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="total_amount[]" class="total_amount" value="" placeholder="" id="" required>
                                                    </td>
                                                    <td>
                                                        {{-- <input type="text" name="category[]" class="" value="{{old('category')}}" placeholder="" id=""> --}}
                                                        
                                                            <select name="category[]" id="category"  class="category" required>
                                                                <option selected disabled>Select Option</option>
                                                                @foreach ($category as $obj)
                                                                    <option value="{{$obj->id}}">{{$obj->name}}</option>
                                                                @endforeach
                                                            </select>
                                                    </td>
                                                    
                                                    <td>
                                                        {{-- <a class="btn btn-danger deleteRow"> <i class="fa fa-trash"></i></a>  --}}
                                                    </td> 
                                                </tr>
                                                {{-- @endif --}}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info" id="submit_btn">Submit</button>
                                <a href="{{route('supplyChain.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>
<noscript type="text/html" id="data_row">
    
    <tr>
        <td>
            <input type="date" name="date[]" class="form-control supply-chain-date" placeholder="" id="" readonly>
        </td>
        <td>
            {{-- <input type="text" name="chq_name[]" class="" value="" placeholder="" id=""> --}}
            <select class="vendor-name required" name="chq_name[]" id="">
                <option value="" selected>Select Vendor</option>
            </select>
        </td>
        <td>
            <input maxlength="13" type="text" name="cnic[]" class="nic" value="" placeholder="" id="" required>
        </td>
        <td>
            <input type="text" name="gt_item_code[]" class="gt_item_code" value="" placeholder="" id="" required>
        </td>
        <td>
            <input type="text" name="nature_of_work[]" class="nature_of_work" value="" placeholder="" id="" required>
        </td>
        <td>
            <input type="text" name="purchaser_person[]" class="purchaser_person" value="" placeholder="" id="" required>
        </td>
        <td>
            <input type="text" name="total_amount[]" class="total_amount" value="" placeholder="" id="" required>
        </td>
        <td>
            {{-- <input type="text" name="category[]" class="" value="" placeholder="" id=""> --}}
            <select name="category[]" id="category"  class="category" required>
                <option selected disabled>Select Option</option>
                @foreach ($category as $obj)
                    <option value="{{$obj->id}}">{{$obj->name}}</option>
                @endforeach
            </select>
        </td>
        <td>
            <a class="btn btn-danger deleteRow"> <i class="fa fa-trash"></i></a> 
        </td> 
    </tr>
</noscript>


@endsection


@push('custom-script')
<script>


// $("#supply_chain_form").validate({
//     rules: {
//         "chq_name[]": "required",
//         "cnic[]" : "required",
//         "gt_item_code[]" : "required",
//         "nature_of_work[]" : "required",
//         "purchaser_person[]" : "required",
//         "total_amount[]" : "required",
//         "category[]" : "required",
//     },
//     messages: {
//         "chq_name[]": "Please select cheque Name",
//         // "cnic[]" : "required",
//         // "gt_item_code[]" : "required",
//         // "nature_of_work[]" : "required",
//         // "purchaser_person[]" : "required",
//         // "total_amount[]" : "required",
//         // "category[]" : "required",
//     }
// });



    $(document).ready(function () {
        
        getDate();
    });

    function getDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 

        today = yyyy + '-' + mm + '-' + dd;
        // console.log(today);
        document.getElementById("supply_chain_date").value = today;
    }

    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        // $( "noscript#data_row > tr > td > select.vendor-name" ).append(option);
        $('tbody').append($("#data_row").html());
        $('.supply-chain-date').val($('#supply_chain_date').val());
        // $('.required').each(function() {
        //     $(this).rules("add",  { required: true  });
        // }); 
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        // console.log($(this).html());
        $('.chq_name_input').val($(this).html());
    });

    var option;
    $('#department').change(function (e) { 
        e.preventDefault();

        let _this = $(this);
        let department_id = _this.val();

        $.ajax({
            type: "POST",
            url: "{{route('getVendorByDepartment')}}",
            data: 
            {
                _token: "{{csrf_token()}}",
                department_id:department_id
            },
            success: function (response) {
                if (response.status) 
                {
                    $(".vendor-name option").remove();
                    let data =  response.data; 
                    option = '';
                    option += '<option value="" selected>Select Vendor</option>';
                    for (var val in data) {
                        option += '<option value="' + data[val].id + '">' + data[val].name + '</option>';
                    }  
                    $('.vendor-name').append(option);
                }
                
            }
        });
    });

    $("table").on("change", ".vendor-name", function(e) {
        
        e.preventDefault();

        let _this = $(this);
        let id = _this.val();

        $.ajax({
            type: "POST",
            url: "{{route('getVendorDetail')}}",
            data: 
            {
                _token: "{{csrf_token()}}",
                id:id
            },
            success: function (response) {
                if (response.status) 
                {
                    // console.log('data',response.data);
                    _this.closest('tr').find('.nic').val(response.data.cnic);
                    _this.closest('tr').find('.gt_item_code').val(response.data.it_code);
                    _this.closest('tr').find('.category').val(response.data.category_id);
                }
                
            }
        });
    });

    $('#submit_btn').click(function (e) {
        e.preventDefault();

        // $('.required').each(function() {
        //     $(this).rules("add", 
        //         {
        //             required: true
        //         })
        // });   
        
        
        Swal.fire({
            title: 'Do you want to save the changes?',
            showCancelButton: true,
            confirmButtonText: `Save`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) 
            {
                // if (!validationCheck()) 
                {
                    $('#supply_chain_form').submit();                    
                }
            } 
            else{
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

    function validationCheck() 
    {
        $('select.required').each(function () {
            // var message = $(this).attr('title');
            if($(this).val() == '' || $(this).val() == 0) {                
                alert('message');
                $(this).focus();
                breakout = true;
                return false;
            }
        });
    }
</script>
@endpush
