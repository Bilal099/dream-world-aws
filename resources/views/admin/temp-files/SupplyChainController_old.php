<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\SupplyChainRepositoryInterface;
use App\Repository\VendorRepositoryInterface;
use App\Repository\PaymentTypeRepositoryInterface;
use App\Repository\DirectorRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\PaymentRepositoryInterface;
use App\Repository\PaymentLogRepositoryInterface;
use App\Repository\DepartmentRepositoryInterface;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;
use PDF;

class SupplyChainController extends Controller
{
    private $supplyChainRepository;
    private $vendorRepository;
    private $paymentTypeRepository;
    private $directorRepository;
    private $categoryRepository;
    private $paymentRepository;
    private $paymentLogRepository;
    private $departmentRepository;



    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(
        PaymentLogRepositoryInterface $paymentLogRepository, 
        VendorRepositoryInterface $vendorRepository, 
        PaymentRepositoryInterface $paymentRepository, 
        SupplyChainRepositoryInterface $supplyChainRepository, 
        PaymentTypeRepositoryInterface $paymentTypeRepository, 
        DirectorRepositoryInterface $directorRepository, 
        DepartmentRepositoryInterface $departmentRepository,
        CategoryRepositoryInterface $categoryRepository
    )
    {
        $this->supplyChainRepository    = $supplyChainRepository;
        $this->vendorRepository         = $vendorRepository;
        $this->paymentTypeRepository    = $paymentTypeRepository;
        $this->directorRepository       = $directorRepository;
        $this->categoryRepository       = $categoryRepository;
        $this->paymentRepository        = $paymentRepository;
        $this->paymentLogRepository     = $paymentLogRepository;
        $this->departmentRepository     = $departmentRepository;

        // for permissions
        $this->permissionView       = 'supply-chain-view';
        $this->permissionCreate     = 'supply-chain-create';
        $this->permissionEdit       = 'supply-chain-edit';
        $this->permissionDelete     = 'supply-chain-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            $vendors  = $this->vendorRepository->vendorsInSupplyChain();
            // dd($vendors[0],$vendors[0]->latestPayment->first());
            // dd($temp[0]->latestPayment);
            // dd($vendors[0]->supplyChain->sum('total_amount'));
            $data = $this->supplyChainRepository->getAllUnPaid();
            $temp_array = [];
            foreach ($data as $key => $value) 
            {
                $temp_array[] = $value->id;
            }
            $complete_payment = $this->paymentRepository->isComplete($temp_array);

            $id_check = [];
            foreach ($complete_payment as $key => $value) 
            {
                $id_check[] = $value->supply_chain_id;
            }
            $is_complete = array_unique($id_check);

            $in_process_payment = $this->paymentRepository->inProcess($temp_array);
            $id_check1 = [];
            foreach ($in_process_payment as $key => $value) 
            {
                $id_check1[] = $value->supply_chain_id;
            }
            $in_process = array_unique($id_check1);

            $data_not_in_process = $this->supplyChainRepository->getPaymentnotInProcess();
            $data_complete = $this->supplyChainRepository->getPaymentIsComplete();
            // dd($data_complete);
            return view('admin.supplyChain.index',\compact('data','is_complete','in_process','data_complete','data_not_in_process','vendors'));
        }
        else
        {
            return view('errors.401');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            $category = $this->categoryRepository->all();
            $department = $this->departmentRepository->all();
            return view('admin.supplyChain.create',compact('category','department'));
        }
        else
        {
            return view('errors.401');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        // $validator = Validator::make($request->all(),[
        //     'date.*'              =>  'required',
        //     'chq_name.*'          =>  'required',
        //     'cnic.*'              =>  'required|string|min:13|max:13',
        //     'gt_item_code.*'      =>  'required',
        //     'nature_of_work.*'    =>  'required',
        //     'purchaser_person.*'  =>  'required',
        //     'total_amount.*'      =>  'required',
        //     'category.*'          =>  'required',
        // ],
        // [
        //     'date.*.required'             => 'This field is required',
        //     'chq_name.*.required'         => 'This field is required',
        //     'cnic.*.required'             => 'This field is required',
        //     'gt_item_code.*.required'     => 'This field is required',
        //     'nature_of_work.*.required'   => 'This field is required',
        //     'purchaser_person.*.required' => 'This field is required',
        //     'total_amount.*.required'     => 'This field is required',
        //     'category.*.required'         => 'This field is required',
        //     'cnic.*.min'             =>'This field must be at least 13 characters.',
        // ]);

        // if($validator->fails())
        // {
        //     return Redirect::back()->withInput($request->input())->withErrors($validator);
        // }

        try {
            $user_id = Auth::user()->id;
            DB::beginTransaction();
            foreach ($request->date as $key => $value) 
            {
                $attributes = [
                    'date'              => $request->date[$key],
                    'vendor_id'       => $request->chq_name[$key],
                    // 'cheque_name'       => $request->chq_name[$key],
                    'cnic_number'       => $request->cnic[$key],
                    'item_code'         => $request->gt_item_code[$key],
                    'work_of_nature'    => $request->nature_of_work[$key],
                    'purchaser_person'  => $request->purchaser_person[$key],
                    'total_amount'      => $request->total_amount[$key],
                    'remaining_amount'  => $request->total_amount[$key],
                    'category_id'       => $request->category[$key],
                    'created_by'        => $user_id,
                ];
                $this->supplyChainRepository->create($attributes);
            }
            DB::commit();
        } 
        catch (\Throwable $th) 
        {
            // DB::rollback();
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('supplyChain.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vendor = $this->vendorRepository->find($id);
        $data = $vendor->supplyChain; 
        // $data = $this->supplyChainRepository->find($id);
        // dd($vendors->supplyChain,$id);
        return view('admin.supplyChain.show',compact('data','vendor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            // $temp_array = array();
            // $temp_array[] = $id; // convert int into array, because the whereIn method parameter get string and array
            // $data = $this->supplyChainRepository->whereIn('id',$temp_array);
            $data = $this->supplyChainRepository->find($id);
            $category = $this->categoryRepository->all();
            return view('admin.supplyChain.edit',compact('data','category'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all(),$id);
        $validator = Validator::make($request->all(),[

            'date'              => 'required',
            // 'chq_name'          => 'required',
            // 'cnic'              => 'required',
            // 'it_code'         => 'required',
            'nature_of_work'    => 'required',
            'purchaser_person'  => 'required',
            'total_amount'      => 'required',
            'category'          => 'required',
        ],
        [
            'date.required'             => 'This field is required',
            'nature_of_work.required'   => 'This field is required',
            'purchaser_person.required' => 'This field is required',
            'total_amount.required'     => 'This field is required',
            'category.required'         => 'This field is required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            $user_id = Auth::user()->id;
            // foreach ($request->id as $key => $value) 
            {
                $attributes = [
                    'date'              => $request->date,
                    'work_of_nature'    => $request->nature_of_work,
                    'purchaser_person'  => $request->purchaser_person,
                    'total_amount'      => $request->total_amount,
                    'category_id'       => $request->category,
                    'updated_by'        => $user_id,
                ];
                $this->supplyChainRepository->update($attributes,$id);
            }
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('supplyChain.show',$request->vendor_id)->with('success','Data is Successfully Added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            // dd('delete');
            try {
                $object = $this->supplyChainRepository->find($id);
                if($object)
                {
                    $this->supplyChainRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Record is not deleted!',
                ]);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Data is Successfully Deleted!',
            ]);
        }
        else
        {
            return view('errors.401');
        }
    }

    public function bulkEdit($json_request)
    {
        if(Auth::user()->hasPermissionTo('supply-chain-bulk-edit'))
        {
            $decode = base64_decode($json_request);
            $ids = json_decode($decode);
            $data = $this->supplyChainRepository->whereIn('id',$ids);
            return view('admin.supplyChain.edit',compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    public function bulkUpdate(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'date.*'              =>  'required',
            // 'chq_name.*'          =>  'required',
            // 'cnic.*'              =>  'required',
            // 'gt_item_code.*'      =>  'required',
            'nature_of_work.*'    =>  'required',
            'purchaser_person.*'  =>  'required',
            'total_amount.*'      =>  'required',
            'category.*'          =>  'required',
        ],
        [
            'date.*.required'             => 'This field is required',
            // 'chq_name.*.required'         => 'This field is required',
            // 'cnic.*.required'             => 'This field is required',
            // 'gt_item_code.*.required'     => 'This field is required',
            'nature_of_work.*.required'   => 'This field is required',
            'purchaser_person.*.required' => 'This field is required',
            'total_amount.*.required'     => 'This field is required',
            'category.*.required'         => 'This field is required',
        ]);

        if($validator->fails())
        {
            // dd(Redirect::back()->withInput($request->input())->withErrors($validator));
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }
        // dd($request->all());

        // try {
            $user_id = Auth::user()->id;
            foreach ($request->id as $key => $value) 
            {
                $attributes = [
                    'date'              => $request->date[$value],
                    // 'cheque_name'       => $request->chq_name[$value],
                    // 'cnic_number'       => $request->cnic[$value],
                    // 'item_code'         => $request->gt_item_code[$value],
                    'work_of_nature'    => $request->nature_of_work[$value],
                    'purchaser_person'  => $request->purchaser_person[$value],
                    'total_amount'      => $request->total_amount[$value],
                    'category_id'       => $request->category[$value],
                    'updated_by'        => $user_id,
                ];
                $this->supplyChainRepository->update($attributes,$value);
            }
        // } 
        // catch (\Throwable $th) 
        // {
        //     return redirect()->back()->with('error','Some thing is wrong!');
        // }
        return redirect()->route('supplyChain.index')->with('success','Data is Successfully Added');
    }

    public function bulkPDF($json_request)
    {
        if(Auth::user()->hasPermissionTo('supply-chain-daily-sheet'))
        {
            $decode = base64_decode($json_request);
            $ids = json_decode($decode);
            // $object = $this->supplyChainRepository->whereIn('id',$ids);
            $object = $this->vendorRepository->whereIn('id',$ids);
            $data = [
                'object' => $object,
            ];
            $pdf = PDF::loadView('admin.supplyChain.pdf', $data);
            return $pdf->setPaper('a4', 'LANDSCAPE')->stream('Supply-Chain-'.now().'.pdf');
            // return $pdf->download('itsolutionstuff.pdf');
        }
        else
        {
            return view('errors.401');
        }
    }

    public function bulkApprove($json_request)
    {
        if(Auth::user()->hasPermissionTo('supply-chain-daily-sheet'))
        {
            $decode = base64_decode($json_request);
            $ids = json_decode($decode);
            $data = $this->vendorRepository->whereIn('id',$ids);
            $payment_type = $this->paymentTypeRepository->all();
            $director = $this->directorRepository->all();
            return view('admin.supplyChain.approve',compact('data','payment_type','director'));
            
        }
        else
        {
            return view('errors.401');
        }
    }

    public function storeApproved(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(),[
            'mode_of_payment.*'              =>  'required',
            'approved_amount.*'          =>  'required|numeric',
            'approved_by.*'              =>  'required',
        ],
        [
            'mode_of_payment.*.required'         => 'This field is required',
            'approved_amount.*.required'         => 'This field is required',
            'approved_by.*.required'             => 'This field is required',
        ]);

        if($validator->fails())
        {
            // dd(Redirect::back()->withInput($request->input())->withErrors($validator));
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }
        // dd($request->all());

        // try {
            DB::beginTransaction();
            $user_id = Auth::user()->id;
            foreach ($request->id as $key => $value) 
            {
                // $attributes = [
                //     'payment_type_id'               => $request->mode_of_payment[$value],
                //     'approved_amount_by_director'   => $request->approved_amount[$value],
                //     'director_id'                   => $request->approved_by[$value],
                //     // 'updated_by'        => $user_id,
                // ];
                $attributes = [
                    'status'        => 1, // 1 means in process
                ];
                $this->supplyChainRepository->update($attributes,$value);

                $attributes = [
                    // 'supply_chain_id'   => $value,
                    'vendor_id'   => $value,
                    'amount_payable'    => $request->approved_amount[$value],
                    'mode_of_payment'   => $request->mode_of_payment[$value],
                    'state'             => 0, // 0 means approve by director
                    'status'            => 0, // 0 means status is in process
                    'created_by'        => $user_id,
                ];
                $object = $this->paymentRepository->create($attributes);
                $log = [
                    'payment_id'    => $object->id,
                    'review_date'   => now(),
                    'reviewer'      => $request->approved_by[$value],
                    'status'        => 1,
                    'reviewer_role' => 'director',
                ];
                $this->paymentLogRepository->create($log);
            }
            DB::commit();
        // } 
        // catch (\Throwable $th) 
        // {
        //     return redirect()->back()->with('error','Some thing is wrong!');
        // }
        return redirect()->route('supplyChain.index')->with('success','Data is Successfully Added');
    }

    public function ajaxCallForSupplyChainNotInProcessPagination(Request $request)
    {
        // dd($request->all());
        $search = $request->query('search', array('value' => '', 'regex' => false));
        $draw = $request->query('draw', 0);
        $start = $request->query('start', 0);
        $length = $request->query('length', 25);
        $order = $request->query('order', array(1, 'asc'));        

        $filter = $search['value'];
        
        $sortColumns = array(
            0 => 'id',
            1 => 'cheque_name',
            2 => 'cnic',
            3 => 'it_code',
        );

        $query = $this->vendorRepository->vendorsInSupplyChainForPagination();

        if (!empty($filter)) 
        {
            $query->where('cheque_name', 'like', '%'.$filter.'%')
                ->orWhere('cnic', 'like', '%'.$filter.'%')
                ->orWhere('it_code', 'like', '%'.$filter.'%');

        }

        $recordsTotal = $query->count();

        $sortColumnName = $sortColumns[$order[0]['column']];

        $query->orderBy($sortColumnName, $order[0]['dir'])
                ->take($length)
                ->skip($start);

        $json = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsTotal,
            'data' => [],
        );

        $details = $query->get();

        $i = $start + 1;
        foreach ($details as $detail) 
        {
            $latest_paymeny = $detail->latestPayment->first();

            if(@$latest_paymeny->status == 1 || @$latest_paymeny == null)
            {
                $temp_amount = @$detail->supplyChain->sum('total_amount'); 
                $temp_paid_amount = @$detail->payment->sum('amount_paid');
                $bill_amount = $temp_amount - $temp_paid_amount;
                $supplyChain = $detail->supplyChain->sortByDesc('id')->first();
                $btn = '';
                $check_box = '<div class="form-group col-md-12">
                        <div class="form-check ">
                        <input type="checkbox" class="form-check-input" name="active" id="" value="" >  '.$i++.'
                        <input type="hidden" name="id[]" value="'.$detail->id.'" class="supply_chain_hidden_field" disabled>
                        </div>
                    </div>';

                if(Auth::user()->hasPermissionTo($this->permissionEdit))
                {
                    $btn .= '<a class="btn btn-secondary btn-sm view-btn ml-1 mt-1" href="'.route('supplyChain.show',$detail->id).'" data-toggle="tooltip" data-placement="top" title="View">
                                    <i class="fa fa-eye"></i>
                                </a>';
                }
                $json['data'][] = [
                    @$check_box,
                    @$supplyChain->date,
                    @$detail->cheque_name,
                    @$detail->cnic,
                    @$detail->it_code,
                    @$supplyChain->work_of_nature,
                    @$supplyChain->purchaser_person,
                    number_format(@$bill_amount),
                    @$supplyChain->category->name,
                    @$btn,
                ];
            }
        }

        return $json;
    }

    public function ajaxCallForSupplyChainCompletePagination(Request $request)
    {
        $search = $request->query('search', array('value' => '', 'regex' => false));
        $draw = $request->query('draw', 0);
        $start = $request->query('start', 0);
        $length = $request->query('length', 25);
        $order = $request->query('order', array(1, 'asc'));        

        $filter = $search['value'];
        
        $sortColumns = array(
            0 => 'id',
            1 => 'cheque_name',
            2 => 'cnic',
            3 => 'it_code',
        );

        $query = $this->vendorRepository->vendorsInSupplyChainForPagination();

        if (!empty($filter)) 
        {
            $query->where('cheque_name', 'like', '%'.$filter.'%')
                ->orWhere('cnic', 'like', '%'.$filter.'%')
                ->orWhere('it_code', 'like', '%'.$filter.'%');

        }

        $recordsTotal = $query->count();

        $sortColumnName = $sortColumns[$order[0]['column']];

        $query->orderBy($sortColumnName, $order[0]['dir'])
                ->take($length)
                ->skip($start);

        $json = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsTotal,
            'data' => [],
        );

        $details = $query->get();

        $i = $start + 1;
        foreach ($details as $detail) 
        {
            // $latest_paymeny = $detail->latestPayment->first();
            $paymeny = $detail->paymentIsComplete->sortByDesc('id');
            $latest_paymeny = $paymeny->first();

            if(@$latest_paymeny->status == 1)
            {
                $temp_amount = @$detail->supplyChain->sum('total_amount'); 
                $temp_paid_amount = @$detail->payment->sum('amount_paid');
                $bill_amount = $temp_amount - $temp_paid_amount;
                $supplyChain = $detail->supplyChain->sortByDesc('id')->first();
              
                $json['data'][] = [
                    @$i++,
                    @$detail->cheque_name,
                    @$detail->cnic,
                    @$detail->it_code,
                    number_format(@$temp_amount),
                    number_format(@$bill_amount),
                    number_format(@$temp_paid_amount),
                ];
            }
        }

        return $json;
    }
}
