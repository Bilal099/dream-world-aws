@extends('admin.layouts.app')
@push('custom-css')

@endpush
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View SMTP</h3>
                        </div>
                        @include('errors.messages')

                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered table-striped" id="data-table">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Title</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 0;
                                    @endphp
                                    @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{++$i}}</td>
                                        <td>{{@$item->slug}}</td>
                                        <td>
                                            <form action="{{ route('admin.settingsDelete')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="id" value="{{@$item->id}}">
                                                <a href="{{route('admin.settingsEdit',@$item->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Update</a>
                                                {{-- <button type="submit" class="btn btn-danger btn-delete btn-sm"><i class="fas fa-trash-alt"></i></button> --}}
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let form = _this.closest('form');
       
        Swal.fire({
            title: 'Do you want to save the changes?',
            showCancelButton: true,
            confirmButtonText: `Save`,
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
            else{
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })
</script>
@endpush