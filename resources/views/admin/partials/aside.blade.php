<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="#" class="brand-link text-center">
    {{-- <img src="{{asset('public/backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
      class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
    <span class="brand-text font-weight-light">Dream World</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('public/backend/images/user-avatar.png')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{@Auth::user()->name}}</a>
      </div>
    </div>

    <!-- SidebarSearch Form -->
    {{-- <div class="form-inline">
<div class="input-group" data-widget="sidebar-search">
<input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
<div class="input-group-append">
<button class="btn btn-sidebar">
<i class="fas fa-search fa-fw"></i>
</button>
</div>
</div>
</div> --}}

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        {{-- @can('language-view')
        <li class="nav-item">
          <a href="{{route('language.index')}}"
            class="nav-link {{ ( Route::currentRouteName() == 'language.index' || Route::currentRouteName() == 'language.create' || Route::currentRouteName() == 'language.edit')? 'active' : '' }}">
            <i class="fas fa-language nav-icon"></i>
            <p>
              Language
            </p>
          </a>
        </li>
        @endcan --}}

        {{-- @can('menu-view')
        <li class="nav-item">
          <a href="{{route('menu.index')}}"
            class="nav-link {{ ( Route::currentRouteName() == 'menu.index' || Route::currentRouteName() == 'menu.create' || Route::currentRouteName() == 'menu.edit')? 'active' : '' }}">
            <i class="fas fa-bars nav-icon"></i>
            <p>
              Menu
            </p>
          </a>
        </li>
        @endcan --}}

        {{-- @can('faq-view')
        <li class="nav-item">
          <a href="{{route('faq.index')}}"
            class="nav-link {{ ( Route::currentRouteName() == 'faq.index' || Route::currentRouteName() == 'faq.create' || Route::currentRouteName() == 'faq.edit')? 'active' : '' }}">
            <i class="fas fa-comments nav-icon"></i>
            <p>
              FAQs
            </p>
          </a>
        </li>
        @endcan --}}

        

        @can('supply-chain-view')
        <li class="nav-item">
          <a href="{{route('supplyChain.index')}}"
            class="nav-link {{Request::segment(2)=='supplyChain'?'active':''}}">
            <i class="fas fa-clipboard-list nav-icon"></i>
            <p>
              Supply Chain
            </p>
          </a>
        </li>
        @endcan

        

        @can('accountant-view')
        <li class="nav-item">
          <a href="{{route('accountant.index')}}"
            class="nav-link {{Request::segment(2)=='accountant'?'active':''}}">
            <i class="fas fa-file-invoice-dollar nav-icon"></i>
            <p>
              Accountant
            </p>
          </a>
        </li>
        @endcan

        
        
        {{-- @can('bank-view')
        <li class="nav-item">
          <a href="{{route('bank.index')}}"
            class="nav-link {{Request::segment(2)=='bank'?'active':''}}">
            <i class="fas fa-university nav-icon"></i>
            <p>
              Banks
            </p>
          </a>
        </li>
        @endcan
        
        @can('bank-account-view')
        <li class="nav-item">
          <a href="{{route('bankAccount.index')}}"
            class="nav-link {{Request::segment(2)=='bankAccount'?'active':''}}">
            <i class="far fa-circle nav-icon"></i>
            <p>
              Bank Accounts
            </p>
          </a>
        </li>
        @endcan --}}
        
        
        
       
        @php
            $account_categories_segments = ['accountCategory','curAccountCategory','subAccountCategory'];
        @endphp
        @if(Gate::check('accountant-category-view') || Gate::check('cur-accountant-category-view') || Gate::check('sub-accountant-category-view') )
        <li class="nav-item {{ ( in_array(Request::segment(2),$account_categories_segments) )? 'menu-is-opening menu-open' : '' }}">
          <a href="#" class="nav-link {{ ( in_array(Request::segment(2),$account_categories_segments) )? 'active' : '' }}">
			      <i class="fas fa-book nav-icon"></i>
            <p>
              Account Categories
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview" style="display: {{ ( in_array(Request::segment(2),$account_categories_segments) )? 'block' : 'none' }};">
            @can('accountant-category-view')
            <li class="nav-item">
              <a href="{{route('accountCategory.index')}}"
                class="nav-link {{Request::segment(2)=='accountCategory'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Account Categories
                </p>
              </a>
            </li>
            @endcan
            @can('cur-accountant-category-view')
            <li class="nav-item">
              <a href="{{route('curAccountCategory.index')}}"
                class="nav-link {{Request::segment(2)=='curAccountCategory'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Cur Account Categories
                </p>
              </a>
            </li>
            @endcan
            
            @can('sub-accountant-category-view')
            <li class="nav-item">
              <a href="{{route('subAccountCategory.index')}}"
                class="nav-link {{Request::segment(2)=='subAccountCategory'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Sub Account Categories
                </p>
              </a>
            </li>
            @endcan
          </ul>
        </li>
        @endif
        
        
        @php
            $general_segments = ['users','director','category','paymentType', 'department', 'vendor'];
        @endphp
        @if(Gate::check('user-view') || Gate::check('director-view') || Gate::check('category-view') || Gate::check('payment-type-view') || Gate::check('department-view') || Gate::check('vendor-view') )
        <li class="nav-item {{ ( in_array(Request::segment(2),$general_segments) )? 'menu-is-opening menu-open' : '' }}">
          <a href="#" class="nav-link {{ ( in_array(Request::segment(2),$general_segments) )? 'active' : '' }}">
            <i class="fa fa-cogs nav-icon" aria-hidden="true"></i>
            <p>
              Generals
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview" style="display: {{ ( in_array(Request::segment(2),$general_segments) )? 'block' : 'none' }};">
            @can('user-view')
            <li class="nav-item">
              <a href="{{route('users.index')}}"
                class="nav-link {{ ( Route::currentRouteName() == 'users.index' || Route::currentRouteName() == 'users.create' || Route::currentRouteName() == 'users.edit')? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Users
                </p>
              </a>
            </li>
            @endcan

            @can('director-view')
            <li class="nav-item">
              <a href="{{route('director.index')}}"
                class="nav-link {{Request::segment(2)=='director'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Directors
                </p>
              </a>
            </li>
            @endcan

            @can('category-view')
            <li class="nav-item">
              <a href="{{route('category.index')}}"
                class="nav-link {{Request::segment(2)=='category'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Categories
                </p>
              </a>
            </li>
            @endcan
    
            @can('payment-type-view')
            <li class="nav-item">
              <a href="{{route('paymentType.index')}}"
                class="nav-link {{Request::segment(2)=='paymentType'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Payment Types
                </p>
              </a>
            </li>
            @endcan
    
            @can('department-view')
            <li class="nav-item">
              <a href="{{route('department.index')}}"
                class="nav-link {{Request::segment(2)=='department'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Departments
                </p>
              </a>
            </li>
            @endcan
    
            @can('vendor-view')
            <li class="nav-item">
              <a href="{{route('vendor.index')}}"
                class="nav-link {{Request::segment(2)=='vendor'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Item Name
                </p>
              </a>
            </li>
            @endcan

            @can('department-bank-view')
            <li class="nav-item">
              <a href="{{route('departmentBank.index')}}"
                class="nav-link {{Request::segment(2)=='departmentBank'?'active':''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>
                  Department Banks
                </p>
              </a>
            </li>
            @endcan
          </ul>
        </li>
        @endif
      </ul>
    </nav>
  </div>
</aside>
