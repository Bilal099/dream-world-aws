@extends('admin.layouts.app')
@push('custom-css')

@endpush
@section('content')
@php
    $total_value_columns = 4;
@endphp
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card mt-3">
                        <div class="card-header d-flex align-items-center">
                            <h4>User Permissions</h4>
                        </div>
                        <form method="POST" action="{{route('auth.user.permissions.post')}}">
                            @csrf
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered permission-table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Module</th>
                                        <th colspan="{{$total_value_columns}}" class="text-center">Permissions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($allPermissions as $key => $item)
                                        @if ($key!='')

                                            @php
                                                $count = 0;
                                            @endphp
                                            <tr>
                                                <td>{{ucwords(str_replace("-", " ", $key))}}</td>
                                                @foreach ($item as $childKey => $chidlItem)
                                                @php
                                                    $permission_name = str_replace("-", " ", $chidlItem['name']);
                                                    $permission_name = ucwords($permission_name);
                                                    $count++;
                                                @endphp
                                                <td class="text-center">
                                                    <div class="icheckbox_square-blue checked" aria-checked="false" aria-disabled="false">
                                                        <div class="checkbox">
                                                            <input type="checkbox" value="1" id="{{$chidlItem['name']}}" name="{{$chidlItem['name']}}" {{in_array($chidlItem['name'], $userPermissions)?'checked':''}} />
                                                            <label for="{{$chidlItem['name']}}">{{$permission_name}}</label>
                                                        </div>
                                                    </div>
                                                </td>
                                                @endforeach
                                                @for ($i = $count; $i < $total_value_columns; $i++)
                                                    <td></td>
                                                @endfor
                                                
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Change Permissions" class="btn btn-primary">
                                <a href="{{route('users.index')}}" class="btn btn-default">Back</a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('admin.partials._modal')
@endsection

@push('custom-script')

<script>

</script>
@endpush