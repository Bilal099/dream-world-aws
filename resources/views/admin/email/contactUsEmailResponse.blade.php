<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3>Dear {{$contactUs->name}}</h3>
    
    <p>We have received your query, we will contact you shortly, thank you for your time.</p>

    <p>Regards Unique Bolts</p>

    {{-- <p><strong>We have received your Query, we will revert you shortly thank you for your time.</strong></p> --}}
</body>
</html>