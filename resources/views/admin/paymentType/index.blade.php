@extends('admin.layouts.app')

@push('custom-css')

<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Payment Types</h3>
                            @can('payment-type-create')
                                <a class="btn btn-success btn-sm float-right" href="{{route('paymentType.create')}}">
                                    Add Record
                                </a>
                            @endcan
                            

                        </div>
                        <form action="" method="GET" id="supply_chain_form">
                            {{-- @csrf --}}
                            <div class="card-body">
                                @include('errors.messages')
                                <table class="table table-bordered table-striped" id="data-table">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($data as $key => $item)
                                        <tr>
                                            
                                            
                                            <td>{{$i++}}</td> 
                                            <td>{{@$item->name}}</td> 
                                            <td class="center">

                                                @can('payment-type-edit')
                                                    <a class="btn btn-primary btn-sm ml-1" href="{{route('paymentType.edit',$item->id)}}" data-toggle="tooltip" data-placement="top" title="edit">
                                                        <i class="fas fa-edit"></i>
                                                    </a>      
                                                @endcan
                                               
                                                @can('payment-type-delete')
                                                    <button class="btn btn-danger btn-sm ml-1 btn-delete" data-id="{{$item->id}}" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                @endcan
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                           
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection


@push('custom-script')


<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let id = _this.data('id'); 
        // let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    url: "paymentType/"+id,
                    type: "DELETE",
                    data: {
                        "id": id,
                        "_token": '{{ csrf_token() }}',
                    },
                    success: function (response) {
                        console.log('response',response['status']);
                        if(response['status'])
                        {
                            $('#success_msg_id').text(response['msg']);
                            $('#success_msg').show();
                            _this.closest('tr').remove();
                        }
                        else{
                            $('#error_msg_id').text(response['msg']);
                            $('#error_msg').show();
                        }
                        $(".alert-dismissible").fadeOut(5000);
                    }
                });
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
