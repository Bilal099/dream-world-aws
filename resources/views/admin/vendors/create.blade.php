@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Item Name</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('vendor.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    <div class="form-group  col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Name</label>
                                        <input id="vendor_name" type="text" name="name" class="form-control" value="{{(old('name')!=null)? (old('name')):(@$data->name)}}" >
                                        @error('name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Cheque Name</label>
                                        <input id="cheque_name" type="text" name="cheque_name" class="form-control" value="{{(old('cheque_name')!=null)? (old('cheque_name')):(@$data->cheque_name)}}" >
                                        @error('cheque_name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    {{-- <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Code</label>
                                        <input type="text" name="code" class="form-control" value="{{(old('code')!=null)? (old('code')):(@$data->code)}}" >
                                        @error('code')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div> --}}
                                    {{-- <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>IT Code</label>
                                        <input type="text" name="it_code" class="form-control" value="{{(old('it_code')!=null)? (old('it_code')):(@$data->it_code)}}" >
                                        @error('it_code')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div> --}}
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>CNIC</label>
                                        <input maxlength="13" type="text" name="cnic" class="form-control" value="{{(old('cnic')!=null)? (old('cnic')):(@$data->cnic)}}" onkeypress="preventNonNumericalInput(event)"  >
                                        @error('cnic')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>NTN</label>
                                        <input maxlength="13" type="text" name="ntn" class="form-control" value="{{(old('ntn')!=null)? (old('ntn')):(@$data->ntn)}}" onkeypress="preventNonNumericalInput(event)"  >
                                        @error('ntn')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Tax Rate</label>
                                        <input type="text" name="tax_rate" class="form-control" value="{{(old('tax_rate')!=null)? (old('tax_rate')):(@$data->tax_rate)}}" >
                                        @error('tax_rate')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Department</label>
                                        <select class="form-control select2" name="department_id" id="department_id">
                                            <option disabled selected>Select Option</option>
                                            @foreach ($department as $item)
                                                <option value="{{$item->id}}" {{old('department_id',@$data->department_id)==$item->id ? 'selected':'' }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('department_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>

                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Mode Of Payment</label>
                                        <select class="form-control select2" name="payment_type_id" id="">
                                            <option disabled selected>Select Option</option>
                                            @foreach ($modeOfPayment as $item)
                                                <option value="{{$item->id}}" {{old('payment_type_id',@$data->payment_type_id)==$item->id ? 'selected':'' }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('payment_type_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Category</label>
                                        <select class="form-control select2" name="category_id" id="">
                                            <option disabled selected>Select Option</option>
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}" {{old('category_id',@$data->category_id)==$item->id ? 'selected':'' }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('category_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Sub Account Category</label>
                                        <select class="form-control select2" data-placeholder="Select Sub Account Category" name="sub_account_category_id" id="sub_account_category_id">
                                            {{-- <option disabled selected>Select Option</option>
                                            @foreach ($accountCategory as $item)
                                                <option value="{{$item->id}}" {{old('sub_account_category_id',@$data->sub_account_category_id)==$item->id ? 'selected':'' }}>{{$item->name}}</option>
                                            @endforeach --}}
                                        </select>
                                        @error('sub_account_category_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('vendor.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>



@endsection


@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();

        $('.select2').select2();

    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
        // $('.export-cargo-detail-date').val($('#of_date').val());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        // console.log($(this).html());
        $('.chq_name_input').val($(this).html());
    });

    $('#vendor_name').blur(function () { 
        $('#cheque_name').val($(this).val());
    });

    
    function preventNonNumericalInput(e) {
        e = e || window.event;
        var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        var charStr = String.fromCharCode(charCode);
        if (!charStr.match(/^[0-9]+$/))
            e.preventDefault();
    }

    var option;
    $('#department_id').change(function (e) { 
        e.preventDefault();

        let _this = $(this);
        let department_id = _this.val();

        $.ajax({
            type: "POST",
            url: "{{route('getSubCategoryByDepartment')}}",
            data: 
            {
                _token: "{{csrf_token()}}",
                department_id:department_id
            },
            success: function (response) {
                if (response.status) 
                {
                    $("#sub_account_category_id option").remove();
                    let data =  response.data; 
                    option = '';
                    option += '<option value="" selected>Select Sub Category</option>';
                    for (var val in data) {
                        option += '<option value="' + data[val].id + '">' + data[val].name + '</option>';
                    }  
                    $('#sub_account_category_id').append(option);
                }
                
            }
        });
    });
   

</script>
@endpush
