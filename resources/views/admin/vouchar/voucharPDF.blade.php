<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table,
        th,
        td {
            border-collapse: collapse;
        }

        td {
            height: 40px !important;
            /* padding-left: 3px; */
            padding-left: 5px !important;
        }

        .h-150{
            height: 150px !important;
        }

        .border {
            border: 1px solid #000;
        }

        .border-right {
            border-right: 1px solid #000;
        }

        .border-left {
            border-left: 1px solid #000;
        }

        .border-top {
            border-top: 1px solid #000;
        }

        .border-bottom {
            border-bottom: 1px solid #000;
        }

        .text-center {
            text-align: center !important;
        }

        .bold {
            font-weight: bold;
        }

        .w-25{
            width: 25% !important;
        }
        
        .w-50{
            width: 50% !important;
        }
        .w-70{
            width: 70% !important;
        }
        .w-15{
            width: 15% !important;
        }

        .vertical-align{
            vertical-align: bottom;
        }
        .left-align{
            text-align: left !important;
        }
        .right-align{
            text-align: right !important;
        }

    </style>
</head>

<body>
    @if ($vch == "DV")
        <h2 class="text-center">DEBIT VOUCHER</h2>
    @elseif($vch == "JV")
        <h2 class="text-center">Journal VOUCHER</h2>
    @elseif($vch == "CV")
        <h2 class="text-center">Credit VOUCHER</h2>
    @endif

    <table style="width: 100%">
        <tbody>
            <tr>
                {{-- <td class="w-70 bold" >Dreamworld Limited</td> --}}
                <td class="w-70 bold" >{{@$department_name}}</td>
                <td class="right-align w-15">Voucher No</td>
                <th class=" w-15"> {{@$voucher_no}}</th>
            </tr>
            <tr>
                <td>Karachi-Pakistan</td>
                <td class="right-align">Voucher Date</td>
                <th class="">{{date('d/m/Y')}}</th>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%">
        <thead>
            <tr>
                <th class="border" colspan="2">Account Title</th>
                <th class="border">A/c Code</th>
                <th class="border">Debit Rs.</th>
                <th class="border">Credit Rs.</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="border-bottom border-left">ON ACCOUNT OF:</td>
                <td class="border-bottom ">{{@$narration}}</td>
                {{-- <td class="border-bottom ">CH # 10606249 ISSUED ON A/C</td> --}}
                <td class="border-bottom "></td>
                <td class="border-bottom "></td>
                <td class="border-bottom border-right"></td>
            </tr>
            <tr>
                <td class="border-bottom border-left">{{@$bank_name}}</td>
                <td class="border-bottom "></td>
                <td class="border-bottom ">{{@$bank_it_code}}</td>
                <td class="border-bottom ">0.00</td>
                <td class="border-bottom border-right">{{number_format(@$amount,2)}}</td>
            </tr>
            {{-- <tr>
                <td class="border-bottom border-left">Bank Al Habib(2) 001819</td>
                <td class="border-bottom "></td>
                <td class="border-bottom ">4902000036</td>
                <td class="border-bottom ">0.00</td>
                <td class="border-bottom border-right">22,500.00</td>
            </tr> --}}
            {{-- <tr>
                <td class="border-bottom border-left">PAKISTAN GOLF FEDERATION</td>
                <td class="border-bottom "></td>
                <td class="border-bottom ">3101000219</td>
                <td class="border-bottom ">22,500.00</td>
                <td class="border-bottom border-right">0.00</td>
            </tr> --}}
            <tr>
                <td class="border-bottom border-left">{{@$cheque_name}}</td>
                <td class="border-bottom "></td>
                <td class="border-bottom ">{{@$it_code}}</td>
                <td class="border-bottom ">{{number_format(@$amount,2)}}</td>
                <td class="border-bottom border-right">0.00</td>
            </tr>

            <tr>
                <th class="border-bottom border-left text-center" colspan="2">Total</th>
                <td class="border-bottom border-left"></td>
                <td class="border-bottom border-left">{{number_format(@$amount,2)}}</td>
                <td class="border-bottom border-right border-left">{{number_format(@$amount,2)}}</td>
            </tr>

            <tr>
                <td class="border-bottom border-left bold">Rupees in word</td>
                <td class="border-bottom  border-right" colspan="4">{{amountInWords(@$amount)}} Only</td>
            </tr>
        </tbody>
    </table>
    <table class="border-left border-right border-bottom" style="width: 100%;">
        <tbody>
            <tr>
                <th class="vertical-align h-150 w-25">Prepared by</th>
                <th class="vertical-align h-150 w-25 border-left">Internal Auditor</th>
                <th class="vertical-align h-150 w-25 border-left">CFO</th>
                <th class="vertical-align h-150 w-25 border-left">Director</th>
            </tr>
        </tbody>
    </table>
</body>

</html>
