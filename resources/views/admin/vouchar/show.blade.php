@extends('admin.layouts.app')

@push('custom-css')

<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    /* th{
        width: 50% !important;
    } */

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Show Vouchar Record</h3>
                        </div>
                        @include('errors.messages')

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">    
                                    <table class="table table-striped " >
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>VCH</th>
                                                <th>Cheque Name</th>
                                                <th>Item Code</th>
                                                <th>Debit</th>
                                                <th>Credit</th>
                                                <th>Description</th>
                                                <th>Select Cheque</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{-- @dd($data->voucharDetail[1]->vendor->cheque_name) --}}
                                            <input type="hidden" id="voucher_id" value="{{$data->id}}" >
                                            @foreach ($data->voucharDetail as $key => $item)
                                            <tr>
                                                <td>{{@$data->vouchar_date}}</td>
                                                <td>{{@$item->VCH}}</td>
                                                <td>{{@$item->vendor->cheque_name}}</td>
                                                <td>{{@$item->vendor->it_code}}</td>
                                                <td>{{number_format(@$item->total_debit,2)}}</td>
                                                <td>{{number_format(@$item->total_credit,2)}}</td>
                                                <td>{{@$item->ent_rem}}</td>
                                                @if (@$item->total_debit != null )
                                                    <form action="{{route('vouchar.chequePrint')}}" method="GET" class="check-print">
                                                        {{-- @csrf --}}
                                                        <input type="hidden" name="voucher_id" value="{{$data->id}}" >
                                                        <input type="hidden" name="debit_voucher_detail_id" class="debit_voucher_detail_id" value="{{@$data->voucharDetail[$key]->id}}" >
                                                        <input type="hidden" name="credit_voucher_detail_id" class="credit_voucher_detail_id" value="{{@$data->voucharDetail[$key+1]->id}}" >
                                                        <input type="hidden" name="cheque_name" class="cheque_name" value="{{@$item->vendor->cheque_name}}">
                                                        <input type="hidden" name="it_code" class="it_code" value="{{@$item->vendor->it_code}}">
                                                        <input type="hidden" name="bank_name" class="bank_name" value="{{@$data->voucharDetail[$key+1]->vendor_id}}">
                                                        <input type="hidden" name="amount" class="amount" value="{{@$item->total_debit}}">
                                                        <input type="hidden" name="VCH" class="VCH" value="{{@$item->VCH}}">
                                                    <td>
                                                        @if (@$item->VCH != "JV")

                                                        <select class="select2 vendor_id cheque_id" name="bank_cheque" id="">
                                                            <option value="-1">Select Option</option>
                                                            @foreach ($cheques as $detail)
                                                                <option value="{{@$detail->id}}" {{(@$detail->default==1)? 'selected':''}} >{{@$detail->bank_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{-- <a class="btn btn-danger btn-sm ml-1" href="{{route('vouchar.chequePrint',$item->id)}}" data-toggle="tooltip" data-placement="top" title="Cheque Print"> --}}
                                                            
                                                            {{-- </a> --}}
                                                            @if (@$item->VCH != "JV")
                                                            <button type="submit" class="btn btn-danger btn-sm ml-1 btn-check-print"><i class="fas fa-book"></i> </button>
                                                            @endif
                                                            <a href="" class="btn btn-sm ml-1 btn-warning btn_voucher"><i class="far fa-file"></i></a>

                                                    </td>
                                                </form>
                                            @else
                                                <td></td>
                                                <td></td>
                                            @endif
                                            </tr>
                                            @endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Narration</label>
                                        <input type="text" name="narration" id="narration" class="form-control" value="{{@$data->narration}}"  readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('accountant.index')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>


    </section>

</div>



@endsection


@push('custom-script')


<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $('.btn-check-print').click(function (e) { 
        e.preventDefault();
        let _this = $(this);
        let cheque_id = _this.closest('tr').find('select').find(':selected').val();

        if(cheque_id == -1)
        {
            alert('Please select cheque');
        }
        else{
            _this.closest('tr').find(".check-print").submit();
        }
    });

    $('.btn_voucher').click(function (e) { 
        e.preventDefault();
        let _this = $(this);
        let row = _this.closest('tr');

        let cheque_name = row.find('.cheque_name').val();
        let bank_name   = row.find('.bank_name').val();
        let amount      = row.find('.amount').val();
        let vch         = row.find('.VCH').val();
        let it_code     = row.find('.it_code').val();
        let voucher_id  = $('#voucher_id').val();
        let debit_voucher_detail_id  = row.find('.debit_voucher_detail_id').val();
        let credit_voucher_detail_id  = row.find('.credit_voucher_detail_id').val();

        let temp_array = {
                            cheque_name:cheque_name, 
                            bank_name:bank_name, 
                            amount:amount, 
                            vch:vch, 
                            it_code:it_code, 
                            voucher_id:voucher_id,
                            debit_voucher_detail_id:debit_voucher_detail_id,
                            credit_voucher_detail_id:credit_voucher_detail_id
                        };
        
        let json_encode = JSON.stringify(temp_array);
        
        var pathparts = location.pathname.split('/');
        console.log('pathparts',pathparts);
        if (location.host == 'localhost') 
        {
            var url = location.origin+'/'+pathparts[1].trim('/')+'/'+pathparts[2].trim('/')+'/admin/accountant/vouchar/voucharPDF/'+json_encode; // for localhost
        }
        else{
            var url = location.origin+'/admin/accountant/vouchar/voucharPDF/'+json_encode; // for live server
        }
        window.location.href = url;
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                form.submit();
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
