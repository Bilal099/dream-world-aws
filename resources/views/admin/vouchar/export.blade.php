@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Export Vouchar Detail By Date</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('vouchar.voucharDetailExport')}}" method="POST" target="_blank">
                                @csrf
                                <div class="card-body row">
                                    <div class="form-group col-md-12">
                                        <label>Name</label>
                                        <select name="department_id" class="form-control select2" id="">
                                            <option value="">Select Option</option>
                                            @foreach ($departments as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('department_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Date</label>
                                        <input type="date" name="vouchar_date" class="form-control" value="" >
                                        @error('vouchar_date')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('department.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>

@endsection


@push('custom-script')
<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
        // $('.export-cargo-detail-date').val($('#of_date').val());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        // console.log($(this).html());
        $('.chq_name_input').val($(this).html());
    });
   

</script>
@endpush
