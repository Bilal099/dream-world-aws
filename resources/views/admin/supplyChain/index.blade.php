@extends('admin.layouts.app')

@push('custom-css')

<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .total-amount {
        background-color: rgb(253, 93, 93) !important;
    }

    .remaining-amount{
        background-color: rgb(93, 253, 101) !important;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Supply Chain</h3>
                            <a class="btn btn-success btn-sm float-right" href="{{route('supplyChain.create')}}">
                                Add Record
                            </a>

                        </div>
                        <form action="" method="GET" id="supply_chain_form">
                            {{-- @csrf --}}
                            <div class="card-body">
                                @include('errors.messages')
                                {{-- testing start --}}
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="data-table-supply-chain">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Date</th>
                                                <th>Cheque Name</th>
                                                <th>Cnic Number</th>
                                                <th>Item Code</th>
                                                <th>Work Of Nature</th>
                                                <th>Purchaser Person</th>
                                                <th>Bill Amount</th>
                                                <th>Category</th>
                                                <th>Partial Amount</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                            <div class="card-footer">
                                @can('supply-chain-daily-sheet')
                                    <a class="btn btn-md btn-danger" id="bulk_pdf">Daily Report</a>
                                @endcan
                                <a class="btn btn-md btn-info" id="bulk_approve">Approve</a>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- Approve by accountant  -->
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Supply Chain Approve By Accountant</h3>
                        </div>
                        <form action="" method="GET" id="supply_chain_form">
                            {{-- @csrf --}}
                            <div class="card-body">
                                @include('errors.messages')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped" id="data-table-complete">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Cheque Name</th>
                                                        <th>Cnic Number</th>
                                                        <th>Item Code</th>
                                                        <th>Total Amount</th>
                                                        <th>Remaining Amount</th>
                                                        <th>Paid Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection


@push('custom-script')


<script>
    $(function () {
        $("#data-table-supply-chain").DataTable({
            "serverSide": true,
            "ajax": {
                url: "{{ route('supplyChain.notInProcess') }}", 
                method: "get"
            },
            "columnDefs" : [{
                'targets': [0,1,5,6,7,8,9], 
                // 'sortable': false
                'orderable': false
            }],
        });

        var table1 = $("#data-table-complete").DataTable({
            "serverSide": true,
            "ajax": {
                url: "{{ route('supplyChain.complete') }}", 
                method: "get"
            },
            "columnDefs" : [{
                'targets': [0,4,5,6], 
                // 'sortable': false
                'orderable': false

            }],
            "rowCallback": function( row, data, index ) {
                $(row).find("td:nth-child(5)").addClass('total-amount');
                $(row).find("td:nth-child(6)").addClass('remaining-amount');
            }  
        });
    });

    $('.form-check-input').change(function (e) { 
        e.preventDefault();
        let _this = $(this);
        if (_this.is(':checked')) 
        {
            _this.closest('div').find('.supply_chain_hidden_field').prop('disabled',false);
        } 
        else {
            _this.closest('div').find('.supply_chain_hidden_field').prop('disabled',true);
        }
    });

    // $('#bulk_edit').click(function(e){
    //     e.preventDefault();
    //     let ids = Array();
    //     let check_count = 0;
    //     $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
    //         let _this = $(this);
    //         if (_this.is(':checked')) 
    //         {
    //             ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
    //             check_count++;
    //         }
    //     });
    //     if (check_count > 0) 
    //     {
    //         let json_request = JSON.stringify(ids);
    //         let url = 'supplyChain/bulkEdit/'+btoa(json_request);
    //         $('#supply_chain_form').attr('action',url);
    //         $('#supply_chain_form').submit();
    //     } 
    //     else {
    //         alert('Select at least one record');    
    //     }
    // });

    $('#bulk_approve').click(function(e){
        e.preventDefault();
        let ids = Array();
        let check_count = 0;
        $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
            let _this = $(this);
            if ($(this).is(':checked')) 
            {
                ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
                check_count++;
            }
        });
        if (check_count > 0) 
        {
            let json_request = JSON.stringify(ids);
            let url = 'supplyChain/approve/'+btoa(json_request);
            $('#supply_chain_form').attr('action',url);
            $('#supply_chain_form').submit();
        } 
        else {
            alert('Select at least one record');    
        }
    });

    $('#bulk_pdf').click(function(e){
        e.preventDefault();
        let ids = Array();
        let partial_amount = Array();
        let check_count = 0;
        $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
            let _this = $(this);
            if ($(this).is(':checked')) 
            {
                ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
                let temp = _this.closest('tr').find('.partial_amount').val();
                if (temp!='') 
                {
                    partial_amount[check_count] = temp;
                }
                else{
                    partial_amount[check_count] = '';
                }
                check_count++;
            }
        });
        console.log('ids',ids);
        console.log('partial_amount',partial_amount);
        if (check_count > 0) 
        {
            let json_request_for_ids = JSON.stringify(ids);
            let json_request_for_partial = JSON.stringify(partial_amount);
            let json_request = json_request_for_ids+'+'+json_request_for_partial;
            console.log('json_request',json_request);
            let url = 'supplyChain/bulkPDF/'+btoa(json_request);
            $('#supply_chain_form').attr('action',url);
            $('#supply_chain_form').attr('target', '_blank').submit();
        } 
        else {
            alert('Select at least one record');    
        }
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let id = _this.data('id'); 
        // let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    url: "supplyChain/"+id,
                    type: "DELETE",
                    data: {
                        "id": id,
                        "_token": '{{ csrf_token() }}',
                    },
                    success: function (response) {
                        console.log('response',response['status']);
                        if(response['status'])
                        {
                            $('#success_msg_id').text(response['msg']);
                            $('#success_msg').show();
                            _this.closest('tr').remove();
                        }
                        else{
                            $('#error_msg_id').text(response['msg']);
                            $('#error_msg').show();
                        }
                        $(".alert-dismissible").fadeOut(5000);
                    }
                });
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
