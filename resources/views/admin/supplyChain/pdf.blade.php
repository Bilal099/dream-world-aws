<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <title>Document</title> --}}
            <!-- CSS Code: Place this code in the document's head (between the 'head' tags) -->
    <style>
        table.GeneratedTable {
            width: 100%;
            background-color: #ffffff;
            border-collapse: collapse;
            border-width: 1px;
            border-color: #0d0b00;
            border-style: solid;
            color: #000000;
        }
        
        table.GeneratedTable td, table.GeneratedTable th {
            border-width: 1px;
            border-color: #0d0b00;
            border-style: solid;
            padding: 5px;
        }
        
        table.GeneratedTable thead {
            background-color: #ffcc00;
        }

        th{
            width: 9% !important;
        }
        td{
            text-align: center !important;
        }
    </style>
</head>

<body>
    <div style="padding: 10px 10px 10px 10px !important">
        <p>Date: <strong>{{custom_date_format(now(),"D jS M, Y")}}</strong></p>
        <h1 style="text-align: center">Daily SupplyChain Report</h1>
    <!-- HTML Code: Place this code in the document's body (between the 'body' tags) where the table should appear -->
    <table class="GeneratedTable">
      <thead>
        <tr>
            <th>Date</th>
            <th>CHQ Name</th>
            <th>NTN/CNIC Number </th>
            <th>GL IT CODE (A/Cs)</th>
            <th>Nature Of Work</th>
            <th>Purchaser Person</th>
            <th>Total Amount </th>
            <th>Partial Amount </th>
            <th>Category</th>
            <th>Mode of Payment</th>
            <th>Approved Amount</th>
            <th>Approved by</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($object as $item)
        @php
            $supplyChain = $item->supplyChain->sortByDesc('id')->first();
        @endphp
        <tr>
            <td>{{@$supplyChain->date}}</td>
            <td>{{@$item->cheque_name}}</td> 
            <td>{{@$item->cnic}}</td>
            <td>{{@$item->it_code}}</td>
            <td>{{@$supplyChain->work_of_nature}}</td>
            <td>{{@$supplyChain->purchaser_person}}</td>
            {{-- <td>{{@$item->remaining_amount}}</td> --}}
            <td>
                @php
                    $temp_amount = @$item->supplyChain->sum('total_amount'); 
                    $temp_paid_amount = @$item->payment->sum('amount_paid');
                    echo number_format($temp_amount - $temp_paid_amount); 
                @endphp
            </td>
            <td>
                @php
                    $key = array_search ($item->id, $ids);
                    
                    echo ($partial_amount[$key]!='')? number_format((int)$partial_amount[$key]):'';
                @endphp
            </td>
            <td>{{@$supplyChain->category->name}}</td>
            {{-- <td>{{@$item->date}}</td>
            <td>{{@$item->vendor->cheque_name}}</td> 
            <td>{{@$item->cnic_number}}</td>
            <td>{{@$item->item_code}}</td>
            <td>{{@$item->work_of_nature}}</td>
            <td>{{@$item->purchaser_person}}</td>
            <td>{{@$item->remaining_amount}}</td>
            <td>{{@$item->category->name}}</td> --}}
            <td></td>
            <td></td>
            <td></td>
        </tr>
    @endforeach
        
      </tbody>
    </table>
    <!-- Codes by Quackit.com -->
    
    
    </div>
</body>

</html>
