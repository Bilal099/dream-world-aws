@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('public/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />
<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    /* th{
        width: 50% !important;
    } */

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Show Supply Chain Single Record</h3>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered" >
                                        <tbody>
                                            <tr>
                                                <th>Fields</th>
                                                <th>Values</th>
                                            </tr>
                                            
                                            <tr>
                                                <th>CHQ Name</th>
                                                <td>{{@$vendor->cheque_name}}</td>
                                            </tr>
                                            <tr>
                                                <th>NTN/CNIC Number</th>
                                                <td>{{@$vendor->cnic}}</td>
                                            </tr>
                                            <tr>
                                                <th>GL IT CODE  (A/Cs)</th>
                                                <td>{{@$vendor->it_code}}</td>
                                            </tr>
                                            <tr>
                                                <th>Total Amount</th>
                                                <td>{{number_format(@$vendor->supplyChain->sum('total_amount'))}}</td>
                                            </tr>
                                            <tr>
                                                <th>Paid Amount</th>
                                                <td>{{number_format(@$vendor->payment->sum('amount_paid'))}}</td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>S.no</th>
                                                <th>Date</th>
                                                <th>Nature Of Work</th>
                                                <th>Purchaser Person</th>
                                                <th>Total Amount</th>
                                                <th>Category</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php($i=0)
                                            @foreach ($data->sortByDesc('id') as $item)    
                                                <tr>
                                                    <td>{{++$i}}</td>
                                                    <td>{{@$item->date}}</td>
                                                    <td>{{@$item->work_of_nature}}</td>
                                                    <td>{{@$item->purchaser_person}}</td>
                                                    <td>{{number_format(@$item->total_amount)}}</td>
                                                    <td>{{@$item->category->name}}</td>
                                                    <td>
                                                        @can('supply-chain-edit')
                                                            <a class="btn btn-primary btn-sm ml-1 mt-1" href="{{route('supplyChain.edit',$item->id)}}" data-toggle="tooltip" data-placement="top" title="edit">
                                                                <i class="fas fa-edit"></i>
                                                            </a>      
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            
                            {{-- <table class="table table-striped table-bordered" >
                                <tbody>
                                    <tr>
                                        <th>Fields</th>
                                        <th>Values</th>
                                    </tr>
                                    <tr>
                                        <th>Date</th>
                                        <td>{{@$data->date}}</td>
                                    </tr>
                                    <tr>
                                        <th>CHQ Name</th>
                                        <td>{{@$data->cheque_name}}</td>
                                    </tr>
                                    <tr>
                                        <th>NTN/CNIC Number</th>
                                        <td>{{@$data->cnic_number}}</td>
                                    </tr>
                                    <tr>
                                        <th>GL IT CODE  (A/Cs)</th>
                                        <td>{{@$data->item_code}}</td>
                                    </tr>
                                    <tr>
                                        <th>Nature Of Work</th>
                                        <td>{{@$data->work_of_nature}}</td>
                                    </tr>
                                    <tr>
                                        <th>Purchaser Person </th>
                                        <td>{{@$data->purchaser_person}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Amount</th>
                                        <td>{{@$data->total_amount}}</td>
                                    </tr>
                                    <tr>
                                        <th>Category</th>
                                        <td>{{@$data->category->name}}</td>
                                    </tr>
                                </tbody>
                            </table> --}}
                        </div>
                        <div class="card-footer">
                            <a href="{{route('supplyChain.index')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>


    </section>

</div>



@endsection


@push('custom-script')
<script src="{{asset('public/backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>

<script>
    $(function () {
        $("#data-table").DataTable();
    });

   

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                form.submit();
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
