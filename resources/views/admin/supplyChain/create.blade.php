@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Payment Type</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <diiv class="row pb-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <label>Department</label>

                                    <select name="deparment_id" id="department" class="form-control select2">
                                        <option disabled selected>Select Department</option>
                                        @foreach ($department as $item)
                                            <option value="{{$item->id}}" data-code="{{$item->code}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-12">
                                    {{-- <label>Sub Account Category</label>
                                    <select class="form-control select2" data-placeholder="Select Sub Account Category" name="sub_account_category_id" id="sub_account_category_id">
                                        
                                    </select>
                                    @error('sub_account_category_id')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror   --}}
                                </div>
                            </diiv>
                            
                            
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Date</label>
                                    <input type="date" name="temp_date" id="temp_date" class="form-control" readonly>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="single" class="control-label uppercase">Item Name</label>
                                    <input list="item_name_list" id="temp_name" type="text" class="form-control" name="temp_name" placeholder="Enter Item Name">
                                    <datalist id="item_name_list">
    
                                    </datalist>
                                    <input type="hidden" id="item_name_id" value="">
                                </div>
                                {{-- <div class="form-group col-md-6">
                                    <label>Cheque Name</label>
                                    <select class="vendor-name required form-control select2" name="temp_name" id="temp_name">
                                        <option value="" selected>Select Item Name</option>
                                    </select>
                                </div> --}}

                                <div class="form-group col-md-6">
                                    <label>NTN/CNIC Number</label>
                                    <input maxlength="13" type="text" name="temp_nic" id="temp_nic" class="form-control" readonly>  
                                </div>

                                <div class="form-group col-md-6">
                                    <label>GL IT CODE  (A/Cs)</label>
                                    <input type="text" name="temp_code" id="temp_code" class="form-control" readonly>  
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Nature Of Work</label>
                                    <input type="text" name="temp_now" id="temp_now" class="form-control">  
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Purchaser Person</label>
                                    <input type="text" name="temp_person" id="temp_person" class="form-control">  
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Total Amount</label>
                                    <input type="text" name="temp_tamount" id="temp_tamount" class="form-control">  
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Category</label>
                                    <select name="temp_category" id="temp_category"  class="form-control category">
                                        <option value="" selected>Select Option</option>
                                        @foreach ($category as $obj)
                                            <option value="{{$obj->id}}">{{$obj->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <a class="btn btn-md btn-warning" id="insert_record">Insert</a>
                                </div>
                            </div>
                            <form id="supply_chain_form" action="{{route('supplyChain.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <table id="supply_chain_table"  class="table table-striped table-bordered" style="">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>CHQ Name</th>
                                                <th>NTN/CNIC Number</th>
                                                <th>GL IT CODE  (A/Cs)</th>
                                                <th>Nature Of Work</th>
                                                <th>Purchaser Person </th>
                                                <th>Total Amount</th>
                                                <th>Category</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                        </tbody>
                                    </table>
                                </div>
                                <button type="submit" class="btn btn-info" id="submit_btn">Submit</button>
                                <a href="{{route('supplyChain.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>

{{-- <input type="hidden" name=""> --}}

@endsection


@push('custom-script')

<script>
    $(document).ready(function () {
        
        getDate();
    });

    // $(document).on('change keyup blur click','#item_name_list', function () {
    //     console.log('item_name_list text',$(this).text());
    // });

    $("#temp_name").keyup(function(e) {
        let val = $(this).val();
        let department_id = $('#department').val();
        let department_code = $('#department :selected').attr('data-code');
        // console.log('department_code',department_code);
        // $(selector).attr(attributeName);
        // console.log('department_id',department_id);
        if (department_id == ''|| department_id == null) 
        {
            Swal.fire('Please Select Department First', '', 'warning')
        } 
        else {
            if(e.keyCode == 32)
            {
                if(val != '')
                {
                    console.log('val',val);
                    // let url = "http://118.103.236.10:8080/Accountapi/api/VCH/getfeedbackdropdowntoall?itname="+val+"&database="+department_code;
                    // route("getVendorByDepartment")
                    $.ajax({
                        type:'POST',
                        // url: url,
                        url: "{{route('getItemDetailFromLocal')}}",
                        data:{ 
                            _token : '{{csrf_token()}}',
                            value : val,
                            database: department_code,
                            department_id: department_id, 
                        },
                        success:function(response) {
                            console.log('response',response);

                            let data = response.data;
                            $('#item_name_list').html("");
                            $.each(data, function (indexInArray, valueOfElement) { 
                                //  console.log('indexInArray',indexInArray);
                                //  console.log('valueOfElement',valueOfElement['IT_NAME']);
                                 $('#item_name_list').append('<option value="'+valueOfElement['IT_NAME']+'" data-department="'+department_id+'" data-sub_code="'+valueOfElement['SUB_CODE']+'" data-it_code="'+valueOfElement['IT_CODE']+'" data-ntno="'+valueOfElement['NTNO']+'" data-cnicno="'+valueOfElement['CNICNO']+'" data-taxrate="'+valueOfElement['TaxRate']+'">');
                            });
                            // if (response.status) 
                            // {
                            //     let data =  response.data; 
                            //     // console.log('data',data);
                            //     $('#item_name_list').html("");
                            //     $.each(data,function(key,val){
                            //         // $('#item_name_list').append('<option id='+val.id+'>'+val.name+'</option>');
                            //         $('#item_name_list').append('<option value="'+val.name+'" id="'+val.id+'">');
                            //     })
                            // }
                        }
                    });
                }
            }
        }
    });

    function getDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 

        today = yyyy + '-' + mm + '-' + dd;
        // console.log(today);
        document.getElementById("temp_date").value = today;
    }
    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
        // $('.export-cargo-detail-date').val($('#of_date').val());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        // console.log($(this).html());
        $('.chq_name_input').val($(this).html());
    });
   
    var option;
    // $('#department').change(function (e) 
    $(document).on("change", "#sub_account_category_id", function(e){ 
        e.preventDefault();

        let _this = $(this);
        let sub_category_id = _this.val();

        $.ajax({
            type: "POST",
            url: "{{route('getVendorBySubCategory')}}",
            data: 
            {
                _token: "{{csrf_token()}}",
                sub_category_id:sub_category_id 
            },
            success: function (response) {
                if (response.status) 
                {
                    $(".vendor-name option").remove();
                    let data =  response.data; 
                    option = '';
                    option += '<option value="" selected>Select Item Name</option>';
                    for (var val in data) {
                        option += '<option value="' + data[val].id + '">' + data[val].name + '</option>';
                    }  
                    $('.vendor-name').append(option);
                }
                
            }
        });
    });

    $(document).on("change", "#temp_name", function(e) {
        
        e.preventDefault();

        let _this = $(this);
        // let id = _this.val();
        let department_id = $('#department').val();

        var item_name = $("#temp_name")[0];
        // console.log('item_name',item_name.value);
        var data_list = $("#item_name_list")[0];
        // console.log('data_list',data_list);
        // return true;
        if(item_name.value.trim() != '')
        {
            var opSelected = data_list.querySelector(`[value="${item_name.value}"]`);
            // alert(opSelected.getAttribute('id'));
            // let id = opSelected.getAttribute('id');
            let sub_code = opSelected.getAttribute('data-sub_code');
            let it_code = opSelected.getAttribute('data-it_code');
            let ntno = opSelected.getAttribute('data-ntno');
            let cnicno = opSelected.getAttribute('data-cnicno');
            let taxrate = opSelected.getAttribute('data-taxrate');
            // console.log(sub_code,it_code,ntno,cnicno,taxrate);
        //     "SUB_CODE": "3101",
        // "IT_CODE": "3101001445",
        // "IT_NAME": "SOIL TESTING SERVICES",
        // "NTNO": "0568452-8           ",
        // "CNICNO": "                    ",
        // "TaxRate": "6"
            $.ajax({
                type: "POST",
                url: "{{route('getVendorDetail')}}",
                data: 
                {
                    _token:         "{{csrf_token()}}",
                    item_name:      item_name.value,
                    sub_code:       sub_code,
                    it_code:        it_code,
                    ntno:           ntno,
                    cnicno:         cnicno,
                    taxrate:        taxrate,
                    department_id:  department_id
                },
                success: function (response) {
                    // console.log('data',response);

                    if (response.status) 
                    {
                        // console.log('data',response.data);
                        $('#temp_nic').val(response.data.cnic);
                        $('#temp_code').val(response.data.it_code);
                        $('#temp_category').val(response.data.category_id);
                        $('#item_name_id').val(response.data.id);
                        // _this.closest('tr').find('#temp_nic').val(response.data.cnic);
                        // _this.closest('tr').find('#temp_code').val(response.data.it_code);
                        // _this.closest('tr').find('#temp_category').val(response.data.category_id);
                    }
                    
                }
            });
        }
    });

    $(document).on('click','#insert_record', function (e) {
        e.preventDefault();
        let data_list;
        let opSelected;
        let temp_name = '';

        let item_name               = $("#temp_name")[0];
        let temp_name_text          = item_name.value.trim();
        console.log('temp_name_text',temp_name_text);
        if(temp_name_text != '')
        {
            // data_list               = $("#item_name_list")[0];
            // opSelected              = data_list.querySelector(`[value="${item_name.value}"]`);
            // temp_name               = opSelected.getAttribute('id');
            temp_name               = $('#item_name_id').val();
        }
        // let temp_name               = $('#temp_name').val();
        // let temp_name_text          = $( "#temp_name option:selected" ).text(); // $('#temp_name').text();
        let temp_date               = $('#temp_date').val();
        let temp_nic                = $('#temp_nic').val();
        let temp_code               = $('#temp_code').val();
        let temp_now                = $('#temp_now').val();
        let temp_person             = $('#temp_person').val();
        let temp_tamount            = $('#temp_tamount').val();
        let temp_category           = $('#temp_category').val();
        let temp_category_text      = $( "#temp_category option:selected" ).text(); //$('#temp_category').text();

        focus
        if(temp_date == '' || temp_date== null)
        {
            $('#temp_date').focus();
        }
        else if(temp_name == '' || temp_name== null)
        {
            $('#temp_name').focus();
        }
        // else if(temp_nic == '' || temp_nic== null)
        // {
        //     $('#temp_nic').focus();
        // }
        // else if(temp_code == '' || temp_code== null)
        // {
        //     $('#temp_code').focus();
        // }
        else if(temp_now == '' || temp_now== null)
        {
            $('#temp_now').focus();
        }
        else if(temp_person == '' || temp_person== null)
        {
            $('#temp_person').focus();
        }
        else if(temp_tamount == '' || temp_tamount== null)
        {
            $('#temp_tamount').focus();
        }
        else if(temp_category == '' || temp_category== null)
        {
            $('#temp_category').focus();
        }
        else{
            var table_row = '';

            table_row = `<tr>
            <td>
                <input type="hidden" name="date[]" value="`+temp_date+`">
                `+temp_date+`
            </td>
            <td>
                <input type="hidden" name="chq_name[]" value="`+temp_name+`">
                `+temp_name_text+`
            </td>
            <td>
                <input type="hidden" name="cnic[]" value="`+temp_nic+`">
                `+temp_nic+`
            </td>
            <td>
                <input type="hidden" name="gt_item_code[]" value="`+temp_code+`">
                `+temp_code+`
            </td>
            <td>
                <input type="hidden" name="nature_of_work[]" value="`+temp_now+`">
                `+temp_now+`
            </td>
            <td>
                <input type="hidden" name="purchaser_person[]" value="`+temp_person+`">
                `+temp_person+`
            </td>
            <td>
                <input type="hidden" name="total_amount[]" value="`+temp_tamount+`">
                `+temp_tamount+`
            </td>
            <td>
                <input type="hidden" name="category[]" value="`+temp_category+`">
                `+temp_category_text+` 
            </td>
            <td>
                <a class="btn btn-danger deleteRow"> <i class="fa fa-trash"></i></a> 
            </td> 
            </tr>`;

            $('tbody').append(table_row);

            $('#temp_name').val("");
            $('#temp_nic').val("");
            $('#temp_code').val("");
            $('#temp_now').val("");
            $('#temp_person').val("");
            $('#temp_tamount').val("");
            $('#temp_category').val("");
        }
    });

    $('#submit_btn').click(function (e) {
        e.preventDefault();
        var rowCount = $('#supply_chain_table tbody tr').length;
        console.log('rowCount',rowCount);
        if (rowCount == 0) 
        {
            Swal.fire('Please insert at least one record', '', 'warning')
        } 
        else {
            Swal.fire({
            title: 'Do you want to save the changes?',
            showCancelButton: true,
            confirmButtonText: `Save`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) 
                {
                    $('#supply_chain_form').submit();
                } 
                else{
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        }
        
    })

    var option;
    $('#department').change(function (e) { 
        e.preventDefault();

        let _this = $(this);
        let department_id = _this.val();

        // $.ajax({
        //     type: "POST",
        //     url: "{{route('getVendorByDepartment')}}",
        //     data: 
        //     {
        //         _token: "{{csrf_token()}}",
        //         department_id:department_id
        //     },
        //     success: function (response) {
        //         if (response.status) 
        //         {
        //             // $("#sub_account_category_id option").remove();
        //             $("#temp_name option").remove();
        //             // $(".vendor-name option").remove();

        //             let data =  response.data; 
        //             option = '';
        //             // option += '<option value="" selected>Select Sub Category</option>';
        //             option += '<option value="" selected>Select Cheque Name</option>';
        //             for (var val in data) {
        //                 option += '<option value="' + data[val].id + '">' + data[val].name + '</option>';
        //             }  
        //             // $('#sub_account_category_id').append(option);
        //             $('#temp_name').append(option);
        //         }
                
        //     }
        // });
    });

</script>
@endpush
