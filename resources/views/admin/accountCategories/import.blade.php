@extends('admin.layouts.app')

@push('custom-css')

<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Import Accountant Categories</h3>
                            {{-- @can('accountant-category-create')
                            <a class="btn btn-success btn-sm float-right" href="{{route('accountCategory.create')}}">
                                Add Record
                            </a>
                            @endcan --}}
                        </div>
                        <div class="card-body">
                            @include('errors.messages')
                            {{-- <table class="table table-bordered table-striped" id="data-table">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Department</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    @endphp
                                    @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{@$item->name}}</td>
                                        <td>{{@$item->code}}</td>
                                        <td>{{@$item->department->name}}</td>
                                        <td class="center">
                                            @can('accountant-category-edit')
                                            <a class="btn btn-primary btn-sm ml-1"
                                                href="{{route('accountCategory.edit',$item->id)}}" data-toggle="tooltip"
                                                data-placement="top" title="edit">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            @endcan

                                            @can('accountant-category-delete')
                                            <form action="{{route('accountCategory.destroy',$item->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm ml-1 btn-delete"
                                                    data-id="{{$item->id}}" data-toggle="tooltip" data-placement="top"
                                                    title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table> --}}
                            <form action="{{ route('mainCategory.import') }}" method="POST" enctype="multipart/form-data">

                                @csrf

                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label>Select Department</label>
                                        <select class="form-control select2" name="department_id" data-placeholder="Select a Department" style="width: 100%;">
                                            {{-- <option disabled selected>Select Option</option> --}}
                                            @foreach ($department as $item)
                                                <option value="{{$item->id}}" {{old('department_id',@$data->department_id)==$item->id ? 'selected':'' }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('department_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  

                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label>Select File</label>
                                        <input type="file" name="file" class="form-control">
                                        @error('file')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror 
                                    </div>
                                </div>
                
                
                                <br>
                
                                <button class="btn btn-success">Import Accountant Categories Data</button>
                
                
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection


@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let id = _this.data('id');
        let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                form.submit();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
