
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="alert bg-danger text-light pb-0" id="modelError" style="display: none;"></div>
    <form action="{{route('users.update',$user->id)}}" method="POST" id="ajaxForm">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Full Name</label>
            <input type="text" name="name" id="name" class="form-control" value="{{old('name',$user->name)}}"/>
            @error('name')
                <p class="text-danger mt-2 mb-0 text-sm">{{$message}}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" class="form-control" value="{{old('email',$user->email)}}"/>
            @error('email')
                <p class="text-danger mt-2 mb-0 text-sm">{{$message}}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="password">Change Password</label>
            <input type="password" name="password" id="password" class="form-control" value="{{old('password')}}"/>
            @error('password')
                <p class="text-danger mt-2 mb-0 text-sm">{{$message}}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="password_confirmation">Confirm New Password</label>
            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" value="{{old('password_confirmation')}}"/>
            @error('password_confirmation')
                <p class="text-danger mt-2 mb-0 text-sm">{{$message}}</p>
            @enderror
        </div>
        <label for="">Is Active</label>
        <div class="form-group clearfix">
            
            <div class="icheck-success d-inline">
                <input type="checkbox"  {{$user->active == 1 ?'checked':''}} name="active" id="checkboxSuccess1">
                <label for="checkboxSuccess1">
                    Active
                </label>
            </div>
            </div>
    </form>
  </div>
  <div class="modal-footer">
    <input type="submit" form="ajaxForm" class="btn btn-primary" value="Update">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  </div>