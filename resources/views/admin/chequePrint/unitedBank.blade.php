<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<style>
		@page { 
            font-size: 14px;
			margin-right: 5px;
			/* margin: 5px; */
			/* margin: 0%; */
			/* size: 5cm 20cm landscape;  */
		}
		.vertical-align-bottom{
            vertical-align: bottom;
        }
		.vertical-align-top{
            vertical-align: top;
        }
		.right-align{
            text-align: right !important;
        }
		table, td{
			/* border: 1px solid #000; */
		}
	</style>
</head>
<body>
	<table style="width: 100%;">
		<tr>
			<td colspan="3"  style="height: 80px"></td>
		</tr>

		<tr style="">
			<td style="width: 35%;padding-right:25px;font-size:8px;" class="right-align">{{$date}}</td>
			<td style="width: 55%" ></td>
			<td style="width: 25%;padding-left:80.5px;" >{{$date}}</td>
		</tr>
		<tr>
			<td style="padding-right:25px;font-size:8px;" class="right-align vertical-align-top">{{$pay}}</td>
			<td colspan="2" style="padding-left: 0px;padding-top: 0px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$pay}} </td>
		</tr>
		<tr>
			<td></td>
			<td style="padding-left:0px;line-height: 1.5;" class="vertical-align-top" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{amountInWords((float)$amount)}} Only</td>
			<td style="padding-left:60px;padding-top: 5px;height:45px" class="vertical-align-top" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{number_format($amount)}}</td>
			{{-- <td style="padding-left:0px;line-height: 2;" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{amountInWords((float)$amount)}} Only</td>
			<td style="padding-left:60px;padding-top: 0px" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{number_format($amount)}}</td> --}}
		</tr>
		<tr>
			<td style="font-size:10px;padding-right:40px;height:25px" class="right-align vertical-align-bottom">{{number_format($amount)}}</td>
			<td> </td>
			<td> </td>
		</tr>
	</table>
</body>
</html>
{{-- <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<style>
		@page { 
            font-size: 14px;
			margin-right: 5px;
			/* margin: 5px; */
			/* margin: 0%; */
			/* size: 5cm 20cm landscape;  */
		}
		table, td{
			/* border: 1px solid #000; */
		}
	</style>
</head>
<body>
	<table style="width: 100%;">
		<tr>
			<td colspan="2"  style="height: 80px"></td>
		</tr>

		<tr style="">
			<td style="width: 75%" ></td>
			<td style="width: 25%;padding-left:80.5px;" >{{$date}}</td>
		</tr>
		<tr>
			<td colspan="2" style="padding-left: 30%;padding-top: 0px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$pay}} </td>
		</tr>
		<tr>
			<td style="width: 85%;padding-left:40%;line-height: 2;" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{amountInWords((float)$amount)}} Only</td>
			<td style="width: 15%;padding-left:75px;padding-top: 0px" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{number_format($amount)}}</td>
		</tr>
	</table>
</body>
</html> --}}