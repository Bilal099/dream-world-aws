@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Department Bank</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('departmentBank.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    <div class="form-group col-md-12">
                                        <label for="single" class="control-label uppercase">Item Name</label>
                                        <input list="item_name_list" id="temp_name" type="text" class="form-control" name="temp_name" placeholder="Enter Item Name" value="{{@$data->vendor->name}}" required>
                                        <datalist id="item_name_list">
        
                                        </datalist>
                                    </div>
                                    {{-- <div class="form-group col-md-12">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="{{(old('name')!=null)? (old('name')):(@$data->name)}}" >
                                        @error('name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div> --}}
                                    {{-- <div class="form-group col-md-12">
                                        <label>Code</label>
                                        <input type="text" name="code" class="form-control" value="{{(old('code')!=null)? (old('code')):(@$data->code)}}" >
                                        @error('code')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div> --}}
                                    <div class="form-group col-md-12">
                                        <div class="form-check ">
                                        <input type="checkbox" class="form-check-input" name="default" id="default"  {{old('default')=="on"||@$data->default==1? 'checked':''}}>  Set as Default.
                                        @error('default')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror 
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="vendor_id" name="vendor_id" value="{{@$data->vendor_id}}">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('departmentBank.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>

@endsection


@push('custom-script')
<script>
   

    $("#temp_name").keyup(function() {
        let val = $(this).val();
        let department_id = $('#department').val();
        // console.log('val',val);
        // console.log('department_id',department_id);
        if(val != '')
        {
            $.ajax({
                type:'POST',
                url:'{{ route("getVendorByName")}}',
                data:{ 
                    _token : '{{csrf_token()}}',
                    value : val,
                },
                success:function(response) {
                    if (response.status) 
                    {
                        let data =  response.data; 
                        // console.log('data',data);
                        $('#item_name_list').html("");
                        $.each(data,function(key,val){
                            $('#item_name_list').append('<option value="'+val.name+'" id="'+val.id+'">'+val.name+'</option>');
                        })
                    }
                }
            });
        }
        
    });

    $(document).on('change','#temp_name', function () {
        let item_name               = $("#temp_name")[0];
        let temp_name_text          = item_name.value.trim();
        if(temp_name_text != '')
        {
            data_list               = $("#item_name_list")[0];
            opSelected              = data_list.querySelector(`[value="${item_name.value}"]`);
            bank_id = opSelected.getAttribute('id');
            $('#vendor_id').val(bank_id);
        }
    });
   

</script>
@endpush
