@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Approve by Accountant</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('accountant.storeApprovedByAccountant')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data))
                                    @foreach ($data as $item)
                                        <input type="hidden" name="id[]" value="{{ @$item->id }}">   
                                    @endforeach
                                @endif
                                <div class="card-body row">
                                   
                                    <div class="col-md-12 col-sm-12">
                                        <table class="table table-striped table-bordered table-responsive" style="margin-top:20px ">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>CHQ Name</th>
                                                    <th>NTN/CNIC Number</th>
                                                    <th>GL IT CODE  (A/Cs)</th>
                                                    <th>Nature Of Work</th>
                                                    <th>Purchaser Person </th>
                                                    <th>Total Amount</th>
                                                    <th>Category</th>
                                                    <th>Mode of Payment</th>
                                                    <th>Approved Amount </th>
                                                    <th>Approved by</th>
                                                    <th>Approved Amount By Accountant</th>
                                                    {{-- <th>Action</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data as $key => $item)
                                                @php
                                                    $latest_paymeny = $item->latestPayment->first();
                                                    $supplyChain = $item->supplyChain->sortByDesc('id')->first();
                                                @endphp
                                                <input type="hidden" name="payment_id[{{@$item->id}}]" value="{{@$latest_paymeny->id}}">
                                                <tr>
                                                    {{-- <td>
                                                        {{@$item->date}}
                                                    </td>
                                                    
                                                    <td>
                                                        {{@$item->vendor->cheque_name}}
                                                    </td>
                                                    <td>
                                                        {{@$item->cnic_number}}
                                                    </td>
                                                    <td>
                                                        {{@$item->item_code}}
                                                    </td>
                                                    <td>
                                                        {{@$item->work_of_nature}}
                                                    </td>
                                                    <td>
                                                        {{@$item->purchaser_person}}
                                                    </td> --}}
                                                    <td>{{@$supplyChain->date}}</td> 
                                                    <td>{{@$item->cheque_name}}</td> 
                                                    <td>{{@$item->cnic}}</td> 
                                                    <td>{{@$item->it_code}}</td>
                                                    <td>{{@$supplyChain->work_of_nature}}</td> 
                                                    <td>{{@$supplyChain->purchaser_person}}</td> 
                                                    <td class="total_amount">
                                                        {{-- {{@number_format($item->remaining_amount)}} --}}
                                                        @php
                                                            $temp_amount = @$item->supplyChain->sum('total_amount'); 
                                                            $temp_paid_amount = @$item->payment->sum('amount_paid');
                                                            echo number_format($temp_amount - $temp_paid_amount); 
                                                        @endphp
                                                    </td>
                                                    <td>
                                                        {{@$supplyChain->category->name}}
                                                    </td>
                                                    <td>
                                                        {{@$latest_paymeny->modeOfPayment->name}}
                                                        {{-- {{@$item->payment->name}} --}}
                                                    </td>
                                                    <td class="amount_payable">
                                                        {{-- {{@number_format($item->approved_amount_by_director)}} --}}
                                                        {{@number_format($latest_paymeny->amount_payable)}}
                                                    </td>
                                                    <td>
                                                        {{@$latest_paymeny->paymentLog[0]->director->name}}
                                                        {{-- {{@$item->director->name}} --}}
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="remaining_amount[{{@$item->id}}]" value="{{@$item->remaining_amount}}">
                                                        <input type="text" name="approved_amount[{{@$item->id}}]" class="approved_amount" value="{{@$item->approved_amount_by_accountant}}" onkeypress="preventNonNumericalInput(event)">
                                                        {{-- old('approved_amount.'.@$item->id) --}}
                                                        @error('approved_amount'.$item->id)
                                                            <p class="text-danger text-sm">{{$message}}</p>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                @endforeach
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                                <button type="submit" class="btn btn-info validate_amount">Update</button>
                                <a href="{{route('accountant.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>


@endsection


@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        $('.chq_name_input').val($(this).html());
    });
   

    function preventNonNumericalInput(e) {
        e = e || window.event;
        var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        var charStr = String.fromCharCode(charCode);
        if (!charStr.match(/^[0-9]+$/))
            e.preventDefault();
    }

    $('.approved_amount').on('change keyup blur',function (e) { 
        let _this = $(this);
        let approve = _this.val();
        // let total = _this.closest('tr').find('.total_amount').text().trim().replace(',','');
        let total = _this.closest('tr').find('.amount_payable').text().trim().replace(',','');
        total = parseInt(total);
        approve = parseInt(approve);

        // console.log('total',total);
        // console.log('approve',approve);
        if(total<approve)
        {
            e.preventDefault();
            _this.val(total);
        }
    });

    var amount_field;
    $('.validate_amount').click(function (e) { 
        e.preventDefault();
        let flag = 0;
        $( ".approved_amount" ).each(function( index ) {
            // console.log( index + ": " + $( this ).val() );
            let _this = $(this); 
            if(_this.val() == 0 || _this.val() == '')
            {
                amount_field = _this;
                flag = 1;
                return false;
            }
        });

        if(flag == 1)
        {
            amount_field.focus();
            alert('Please insert a valid amount');
        }
        else{
            $('form').submit();
        }
    });
</script>
@endpush
