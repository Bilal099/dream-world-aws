@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('public/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />
<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Account Records</h3>
                            {{-- <a class="btn btn-success btn-sm float-right" href="{{route('supplyChain.create')}}">
                                Add Record
                            </a> --}}

                        </div>
                        <form action="" method="GET" id="supply_chain_form">
                            {{-- @csrf --}}
                            <div class="card-body">
                                @include('errors.messages')
                                <table class="table table-bordered table-striped table-responsive" id="data-table">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Date</th>
                                            <th>Cheque Name</th>
                                            <th>Cnic Number</th>
                                            <th>Item Code</th>
                                            <th>Work Of Nature</th>
                                            <th>Purchaser Person</th>
                                            <th>Amount</th>
                                            {{-- <th>Remaining Amount</th> --}}
                                            <th>Category</th>
                                            <th>Mode of Payment</th>
                                            <th>Approved By</th>
                                            <th>Approve Amount</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($data as $key => $item)
                                        {{-- @if ($item->director_id != null) --}}
                                        @if (in_array($item->id,$in_process))

                                        <tr>
                                            <td>
                                                <div class="form-group col-md-12">
                                                    <div class="form-check ">
                                                    <input type="checkbox" class="form-check-input" name="active" id="" value="" >  {{$i++}}
                                                    <input type="hidden" name="id[]" value="{{$item->id}}" class="supply_chain_hidden_field" disabled>
                                                    {{-- supply_chain_check_box --}}
                                                    </div>
                                                </div>
                                            </td>
                                            
                                            <td>{{@$item->date}}</td> 
                                            <td>{{@$item->vendor->cheque_name}}</td> 
                                            {{-- <td>{{@$item->cheque_name}}</td>  --}}
                                            <td>{{@$item->cnic_number}}</td> 
                                            <td>{{@$item->item_code}}</td> 
                                            <td>{{@$item->work_of_nature}}</td> 
                                            <td>{{@$item->purchaser_person}}</td> 
                                            {{-- <td>{{@number_format($item->total_amount)}}</td>  --}}
                                            <td>{{@number_format($item->remaining_amount)}}</td>
                                            <td>{{@$item->category->name}}</td> 
                                            <td>{{@$item->payment[0]->modeOfPayment->name}}</td>
                                            <td>
                                                {{@$item->payment[0]->paymentLog[0]->director->name}}
                                                {{-- {{@$item->director->name}} --}}
                                            </td>
                                            <td>
                                                {{@$item->payment[0]->amount_payable}}
                                                
                                            </td>
                                            {{-- <td>{{@$item->approved_amount_by_accountant}}</td> --}}
                                            <td class="">
                                                {{-- @can('accountant-view')
                                                    <a class="btn btn-secondary btn-sm view-btn" href="{{route('supplyChain.show',$item->id)}}" data-toggle="tooltip" data-placement="top" title="View">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                @endcan --}}

                                                {{-- @can('accountant-edit')
                                                    <a class="btn btn-primary btn-sm ml-1" href="{{route('accountant.edit',$item->id)}}" data-toggle="tooltip" data-placement="top" title="edit">
                                                        <i class="fas fa-edit"></i>
                                                    </a>      
                                                @endcan --}}
                                               
                                                {{-- @can('accountant-delete')
                                                    <button class="btn btn-danger btn-sm ml-1 btn-delete" data-id="{{$item->id}}" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                @endcan --}}
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                                {{-- @can('accountant-bulk-edit')
                                    <a class="btn btn-md btn-warning" id="bulk_edit">Bulk Edit</a>
                                @endcan

                                @can('accountant-daily-sheet')
                                    <a class="btn btn-md btn-danger" id="bulk_pdf">Daily Report</a>
                                @endcan--}}

                                <a class="btn btn-md btn-info" id="bulk_approve">Approve Amount By Accountant</a> 

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection


@push('custom-script')
<script src="{{asset('public/backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $('.form-check-input').change(function (e) { 
        e.preventDefault();
        let _this = $(this);
        if (_this.is(':checked')) 
        {
            _this.closest('div').find('.supply_chain_hidden_field').prop('disabled',false);
        } 
        else {
            _this.closest('div').find('.supply_chain_hidden_field').prop('disabled',true);
        }
    });

    $('#bulk_edit').click(function(e){
        e.preventDefault();
        let ids = Array();
        let check_count = 0;
        $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
            let _this = $(this);
            if (_this.is(':checked')) 
            {
                ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
                check_count++;
            }
        });
        if (check_count > 0) 
        {
            let json_request = JSON.stringify(ids);
            let url = 'supplyChain/bulkEdit/'+btoa(json_request);
            $('#supply_chain_form').attr('action',url);
            $('#supply_chain_form').submit();
        } 
        else {
            alert('Select at least one record');    
        }
    });

    $('#bulk_approve').click(function(e){
        e.preventDefault();
        let ids = Array();
        let check_count = 0;
        $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
            let _this = $(this);
            if ($(this).is(':checked')) 
            {
                ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
                check_count++;
            }
        });
        if (check_count > 0) 
        {
            let json_request = JSON.stringify(ids);
            let url = 'accountant/approve/'+btoa(json_request);
            $('#supply_chain_form').attr('action',url);
            $('#supply_chain_form').submit();
        } 
        else {
            // alert('Select at least one record');    
            Swal.fire('Select at least one record', '', 'warning')

        }
    });

    $('#bulk_pdf').click(function(e){
        e.preventDefault();
        let ids = Array();
        let check_count = 0;
        $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
            let _this = $(this);
            if ($(this).is(':checked')) 
            {
                ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
                check_count++;
            }
        });
        if (check_count > 0) 
        {
            let json_request = JSON.stringify(ids);
            let url = 'supplyChain/bulkPDF/'+btoa(json_request);
            $('#supply_chain_form').attr('action',url);
            $('#supply_chain_form').attr('target', '_blank').submit();
            // $('#supply_chain_form').submit();
        } 
        else {
            alert('Select at least one record');    
        }
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let id = _this.data('id'); 
        // let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    url: "supplyChain/"+id,
                    type: "DELETE",
                    data: {
                        "id": id,
                        "_token": '{{ csrf_token() }}',
                    },
                    success: function (response) {
                        console.log('response',response['status']);
                        if(response['status'])
                        {
                            $('#success_msg_id').text(response['msg']);
                            $('#success_msg').show();
                            _this.closest('tr').remove();
                        }
                        else{
                            $('#error_msg_id').text(response['msg']);
                            $('#error_msg').show();
                        }
                    }
                });
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
