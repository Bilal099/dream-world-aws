@extends('website.layouts.app')
@php
    $menu     = array();
    $menu_url = array();
    foreach ($menuList as $item) 
    {
        $menu[$item->slug]     = $item->name;
        $menu_url[$item->slug] = $item->link;
    }
@endphp

@section('content')
@include('website.variables.menuVariable')

    <h1>{{@$menu['home']}}</h1>
@endsection