<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Multi Language</title>

    @include('website.partials.style')
</head>

<body>
{{-- @php
    $menu     = array();
    $menu_url = array();
    foreach ($menuList as $item) 
    {
        $menu[$item->slug]     = $item->name;
        $menu_url[$item->slug] = $item->link;
    }
@endphp --}}

{{-- <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">WebSiteName</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{($menu_url['home']!=null)? url($menu_url['home']):"#"}}">{{@$menu['home']}}</a></li>
        @if (count($lang)>1)
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Language<span class="caret"></span></a>
            <ul class="dropdown-menu">
                @foreach ($lang as $item)
                    <li><a href="{{route('lang', $item->code)}}">{{$item->language}}</a></li>
                @endforeach
            </ul>
          </li>
        @endif
        
        <li><a href="{{($menu_url['about']!=null)? url($menu_url['about']):"#"}}">{{@$menu['about']}}</a></li>
        <li><a href="{{($menu_url['contact']!=null)? url($menu_url['contact']):"#"}}">{{@$menu['contact']}}</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{($menu_url['sign-up']!=null)? url($menu_url['sign-up']):"#"}}"><span class="glyphicon glyphicon-user"></span> {{@$menu['sign-up']}}</a></li>
        <li><a href="{{($menu_url['login']!=null)? url($menu_url['login']):"#"}}"><span class="glyphicon glyphicon-log-in"></span> {{@$menu['login']}}</a></li>
      </ul>
    </div>
  </nav> --}}

    <div class="container">
        {{-- <h3>Basic Navbar Example</h3>
        <p>A navigation bar is a navigation header that is placed at the top of the page.</p> --}}
        @yield('content')
    </div>
    

    @include('website.partials.script')
</body>

</html>
