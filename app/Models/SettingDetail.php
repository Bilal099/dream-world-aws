<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Setting;

class SettingDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'setting_id',
        'key',
        'value'
    ];

    public function setting()
    {
        return $this->belongsTo(Setting::class);
    }
}
