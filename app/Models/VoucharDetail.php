<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VoucharDetail extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'vouchar_id',
        'vendor_id',
        'VCH',
        'en_no',
        'ent_rem',
        'it_code',
        'total_credit',
        'total_debit',
        'created_by',
        'updated_by',
        'deleted_by',
        'department_id',
    ];

    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor', 'vendor_id', 'id');
    }
}
