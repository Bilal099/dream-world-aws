<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Payment;
use App\Models\PaymentType;

class SupplyChain extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'date',
        'cheque_name',
        'cnic_number',
        'item_code',
        'work_of_nature',
        'purchaser_person',
        'total_amount',
        'remaining_amount',
        'category_id',
        'payment_type_id',
        'director_id',
        'approved_amount_by_director',
        'accountant_id',
        'approved_amount_by_accountant',
        'created_by',
        'updated_by',
        'deleted_by',
        'vendor_id',
    ];

    public function director()
    {
        return $this->belongsTo('App\Models\Director', 'director_id', 'id');
    }

    // public function payment()
    // {
    //     return $this->belongsTo('App\Models\PaymentType', 'payment_type_id', 'id');
    // }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }
    public function payment()
    {
        return $this->hasMany('App\Models\Payment', 'supply_chain_id', 'id')->where('status',0);
    }

    /**
     * Get the user that owns the SupplyChain
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor', 'vendor_id', 'id');
    }

    /**
     * Get all of the comments for the SupplyChain
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentComplete()
    {
        // return $this->hasMany(Comment::class, 'foreign_key', 'local_key');
        return $this->hasMany('App\Models\Payment', 'supply_chain_id', 'id')->where('status',1);
    }

    public function paymentNotProceed()
    {
        // return $this->hasMany(Comment::class, 'foreign_key', 'local_key');
        return $this->hasMany('App\Models\Payment', 'supply_chain_id', 'id');
    }
}
