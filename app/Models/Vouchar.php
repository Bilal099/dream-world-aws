<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vouchar extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'payment_id',
        'vouchar_date',
        'VCH',
        'EN_No',
        'narration',
        'total_credit',
        'total_debit',
        'created_by',
        'updated_by',
        'deleted_by',
        'department_id',
    ];

    /**
     * Get the user that owns the Vouchar
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id', 'id');
    }

    /**
     * Get all of the comments for the Vouchar
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function voucharDetail()
    {
        return $this->hasMany(VoucharDetail::class, 'vouchar_id', 'id');
    }

    /**
     * Get the department that owns the Vouchar
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}
