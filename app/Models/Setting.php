<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SettingDetail;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug'
    ];

    public function settingdetail()
    {
        return $this->hasMany(SettingDetail::class);

    }
}

