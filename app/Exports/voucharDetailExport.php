<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use app\Models\VoucharDetail;

class voucharDetailExport implements FromQuery,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return VoucharDetail::all();
    // }

    private $id_array;
    public function __construct($id_array)
    {
        $this->id_array = $id_array;
    }

    public function headings(): array
    {
        return [
            'sno',	
            'VCH',	
            'EN_No',	
            'EntRem',	
            'It_Code1',	
            'It_Code',	
            'Debit',	
            'Credit',	
            'DeletedStatus',	
            'ReceiptID',	
            'InstEntryNo',	
            'APLSNo',	
            'CheckNo',	
            'isBank2Voucher',	
            'PRVVch',	
            'PRVNo',	
            'TrStatus',	
            'invNo',	
            'invdate',								

        ];
    }

    public function query()
    {
        return VoucharDetail::query()->whereIn('vouchar_id',$this->id_array);
    }

    public function map($voucharDetail): array
    {
        return [
            '',                                                                     // 'sno',
            $voucharDetail->VCH,                                                    // 'VCH',
            $voucharDetail->vouchar_id,                                             // 'EN_No',
            $voucharDetail->ent_rem,                                                // 'EntRem',
            'NULL',                                                                 // 'It_Code1',
            $voucharDetail->it_code,                                                // 'It_Code',
            ($voucharDetail->total_debit!=null)? $voucharDetail->total_debit:0,     // 'Debit',
            ($voucharDetail->total_credit!=null)? $voucharDetail->total_credit:0,   // 'Credit',
            'no',                                                                   // 'DeletedStatus',
            'NULL',                                                                 // 'ReceiptID',
            'NULL',                                                                 // 'InstEntryNo',
            'NULL',                                                                 // 'APLSNo',
            'NULL',                                                                 // 'CheckNo',
            'NULL',                                                                 // 'isBank2Voucher',
            'NULL',                                                                 // 'PRVVch',
            'NULL',                                                                 // 'PRVNo',
            'NULL',                                                                 // 'TrStatus',
            'NULL',                                                                 // 'invNo',
            'NULL',                                                                 // 'invdate',
        ];
    }
//     vouchar_id
// vendor_id
// VCH
// en_no
// ent_rem
// it_code
// total_credit
// total_debit
// created_by
// updated_by
// deleted_by
}
