<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class voucharMultipleExport implements WithMultipleSheets
{
    private $id_array;
    public function __construct($id_array)
    {
        $this->id_array = $id_array;
    }

    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new voucharExport($this->id_array);

        $sheets[] = new voucharDetailExport($this->id_array);

        return $sheets;
    }
}
