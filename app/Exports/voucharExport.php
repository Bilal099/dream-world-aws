<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use app\Models\Vouchar;

// FromCollection, 
class voucharExport implements FromQuery,WithMapping, WithHeadings
{
    // public function collection()
    // {
    //     return Vouchar::all();
    // }
    private $id_array;
    public function __construct($id_array)
    {
        $this->id_array = $id_array;
    }

    public function headings(): array
    {
        return [
            'Date',	
            'VCH',	
            'EN_No',	
            'Narr',	
            'drawn',	
            'TotDR',	
            'TotCR',	
            'Owner',	
            'Prn',	
            'watch',	
            'Status',	
            'User1',	
            'DeletedStatus',	
            'InstEntryNo',	
            'ReceiptID',	
            'PRVVch',	
            'PRVNo',	
            'APLStatus',	
            'APLSNo',	
            'PostDated',	
            'CheckNo',	
            'TrStatus',	
            'posslipno',	
            'miscpayid',	
            'resno',	
            'locationid',	
            'eventid',
        ];
    }

    public function query()
    {
        return Vouchar::query()->whereIn('id',$this->id_array);
    }

    public function map($vouchar): array
    {
        return [
            $vouchar->vouchar_date,    //'Date',		
            $vouchar->VCH,             // 'VCH',		
            $vouchar->id,           // 'EN_No',		
            $vouchar->narration,       // 'Narr',		
            '',                        // 'drawn',		
            $vouchar->total_debit,     // 'TotDR',		
            $vouchar->total_credit,    // 'TotCR',		
            'NULL',                   // 'Owner',		
            'NULL',                     // 'Prn',		
            'NULL',                   // 'watch',		
            'Save',                  // 'Status',		
            'NULL',                   // 'User1',		
            'no',                      // 'DeletedStatus',		
            'NULL',                    // 'InstEntryNo',		
            'NULL',                    // 'ReceiptID',		
            'NULL',                    // 'PRVVch',		
            'NULL',                    // 'PRVNo',		
            'NULL',                    // 'APLStatus',		
            'NULL',                    // 'APLSNo',		
            'NULL',                    // 'PostDated',		
            'NULL',                    // 'CheckNo',		
            'NULL',                    // 'TrStatus',		
            'NULL',                    // 'posslipno',		
            'NULL',                    // 'miscpayid',		
            'NULL',                    // 'resno',		
            'NULL',                    // 'locationid',		
            'NULL',                    // 'eventid',
        ];
    }

    // payment_id
    // vouchar_date
    // VCH
    // EN_No
    // narration
    // total_credit
    // total_debit
    // created_by
    // updated_by
    // deleted_by
    // department_id
}
