<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Repository\FaqRepositoryInterface;

class FaqList extends Component
{
    public $data;

    public function mount(FaqRepositoryInterface $faqRepository)
    {
        $this->data = $faqRepository->all();
    }

    public function delete(FaqRepositoryInterface $faqRepository, $id)
    {
        $faqRepository->delete($id);
        $this->data = $faqRepository->all();
        session()->flash('message', 'FAQ successfully deleted.');
    }

    public function render()
    {
        return view('livewire.faq-list');
    }
}
