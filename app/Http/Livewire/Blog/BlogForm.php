<?php

namespace App\Http\Livewire\Blog;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Repository\BlogRepositoryInterface;
use App\Repository\LanguageRepositoryInterface;
use App\Services\UploadFile;
use DB;

class BlogForm extends Component
{
    use WithFileUploads;
    // Repository Variables
    private $blogRepository;
    private $languageRepository;

    // Global Variables
    public $_id;
    public $languages;
    public $tabActive;
    
    // Form Variables 
    public $title;
    public $description;
    public $active;
    public $image;
    public $imageName;


    
    // Form Validations
    protected $rules = [
        'title.*'           => 'required',
        'description.*'     => 'required',
    ];
    
    protected $messages = [
        'title.*.required'          => 'This title cannot be empty.',
        'description.*.required'    => 'This description cannot be empty.',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatedPhoto()
    {
        $this->validate([
            'image' => 'image|max:1024', // 1MB Max
        ]);
    }

    public function mount(BlogRepositoryInterface $blogRepository, LanguageRepositoryInterface $languageRepository, $id)
    {
        $this->languages = $languageRepository->all();
        $this->tabActive = $this->languages[0]->code;
        if($id){
            $this->_id = $id;
            $blog = $blogRepository->find($id);
            $this->active   = $blog->active;
            $this->imageName    = $blog->image;
        }
        foreach ($this->languages as $index => $language) {
            $this->title[$language->code]       = isset($blog)?$blog->translate(@$language->code)->title:"";
            $this->description[$language->code] = isset($blog)?$blog->translate(@$language->code)->description:"";
        }
        
    }

    public function save(BlogRepositoryInterface $blogRepository)
    {
        // dd($this->description);
        $validatedData = $this->validate();   
        // try {
            $attributes = $this->setAttribute();
            $blogRepository->createBlog($attributes);
            $this->title        = array();
            $this->description  = array();
            $this->imageName    = null;
            unset($this->image);
            $this->active       = 0;
        // } 
        // catch (\Throwable $th) {
        //     session()->flash('error', 'Some problem occur!');
        // }
        session()->flash('success', 'Blog successfully created.');
        return redirect()->to('admin/blog');
    }

    public function update(BlogRepositoryInterface $blogRepository)
    {
        $validatedData = $this->validate();
        $attributes = $this->setAttribute();
        $blogRepository->update($attributes,$this->_id);
        if($this->image!=null)
        {
            unset($this->image);
        }
        session()->flash('success', 'FAQ successfully Updated.');
    }
    
    public function render()
    {
        return view('livewire.blog.blog-form');
    }

    public function setAttribute()
    {
        
        $attributes = array();
        foreach ($this->languages as $key => $value){
            $attributes[$value->code] = array(
                'title'         => $this->title[$value->code],
                'description'   => $this->description[$value->code],
            );
        }
        $attributes['active']   = $this->active?1:0;
        if($this->image!=null)
        {
            $attributes['image']    = $this->imageUpload();
        }
        return $attributes;
    }

    public function imageUpload()
    {
        $temp = $this->image->store('public/blog');
        $temp =  str_replace("public/","",$temp);
        return  $temp; 
    }

    public function changeTabs($languageCode)
    {
        $this->tabActive = $languageCode;
    }
}
