<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\DepartmentRepositoryInterface;
use App\Repository\VendorRepositoryInterface;
use App\Repository\PaymentTypeRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\AccountCategoryRepositoryInterface;
use App\Repository\SubAccountCategoryRepositoryInterface;

use App\Imports\ItemNameImport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class VendorController extends Controller
{
    private $departmentRepository;
    private $vendorRepository;
    private $paymentTypeRepository;
    private $categoryRepository;
    private $accountCategoryRepository;
    private $subAccountCategoryRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(
        VendorRepositoryInterface $vendorRepository, 
        DepartmentRepositoryInterface $departmentRepository,
        PaymentTypeRepositoryInterface $paymentTypeRepository,
        CategoryRepositoryInterface $categoryRepository,
        AccountCategoryRepositoryInterface $accountCategoryRepository,
        SubAccountCategoryRepositoryInterface $subAccountCategoryRepository
    )
    {
        $this->departmentRepository     = $departmentRepository;
        $this->vendorRepository         = $vendorRepository;
        $this->paymentTypeRepository    = $paymentTypeRepository;
        $this->categoryRepository       = $categoryRepository;
        $this->accountCategoryRepository = $accountCategoryRepository;
        $this->subAccountCategoryRepository = $subAccountCategoryRepository;

        // for permissions
        $this->permissionView       = 'vendor-view';
        $this->permissionCreate     = 'vendor-create';
        $this->permissionEdit       = 'vendor-edit';
        $this->permissionDelete     = 'vendor-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \set_time_limit(0);
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            // $data = $this->vendorRepository->allDesc();
            // $data = $this->vendorRepository->findByColumnMultiple('id',10);
            $data = null;
            return view('admin.vendors.index',\compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            $department = $this->departmentRepository->all();
            $modeOfPayment = $this->paymentTypeRepository->all();
            $category = $this->categoryRepository->all();
            $accountCategory = $this->accountCategoryRepository->allDesc();
            return view('admin.vendors.create',compact('department','modeOfPayment','category','accountCategory'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(),[
            'name'              =>  'required',
            'cheque_name'       => 'required',
            // 'it_code'           => 'required',
            'cnic'          => 'nullable|string|min:13|max:13',
            'ntn'          => 'nullable|string|min:13|max:13',
            'tax_rate'          => 'nullable',
            'department_id'     => 'required',
            'payment_type_id'   => 'nullable',
            'category_id'       => 'nullable',
            'sub_account_category_id'   => 'required',
        ],[
            'name.required'             => 'This field is required',
            'cheque_name.required'      => 'This field is required',
            // 'it_code.required'          => 'This field is required',
            // 'cnic.required'         => 'This field is required',
            // 'tax_rate.required'         => 'This field is required',
            'department_id.required'    => 'This field is required',
            // 'payment_type_id.required'  => 'This field is required',
            // 'category_id.required'      => 'This field is required',
            'sub_account_category_id.required'      => 'This field is required',
            'cnic.min'              =>'This field must be at least 13 characters.',
            // 'cnic.min'              =>'This field must be at least 13 characters.',

        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }
        $sub_details = $this->subCode($request->sub_account_category_id,$request->department_id);
        // dd($temp);
        $it_code = $sub_details['it_code'];
        $sub_code = $sub_details['code'];

        $attributes = [];
        
        $attributes['name']             = (isset($request->name)?                       ($request->name!=null?                      $request->name:                     null ):null);
        $attributes['cheque_name']      = (isset($request->cheque_name)?                ($request->cheque_name!=null?               $request->cheque_name:              null ):null);
        $attributes['cnic']             = (isset($request->cnic)?                       ($request->cnic!=null?                      $request->cnic:                     null ):null);
        $attributes['ntn']              = (isset($request->ntn)?                        ($request->ntn!=null?                       $request->ntn:                      null ):null);
        $attributes['tax_rate']         = (isset($request->tax_rate)?                   ($request->tax_rate!=null?                  $request->tax_rate:                 null ):null);
        $attributes['department_id']    = (isset($request->department_id)?              ($request->department_id!=null?             $request->department_id:            null ):null);
        $attributes['payment_type_id']  = (isset($request->payment_type_id)?            ($request->payment_type_id!=null?           $request->payment_type_id:          null ):null);
        $attributes['category_id']      = (isset($request->category_id)?                ($request->category_id!=null?               $request->category_id:              null ):null);
        $attributes['sub_category_id']  = (isset($request->sub_account_category_id)?    ($request->sub_account_category_id!=null?   $request->sub_account_category_id:  null ):null);
        $attributes['it_code']          = $it_code;
        $attributes['sub_code']         = $sub_code;


        // dd($attributes);
        try {
            
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $attributes['updated_by'] = $user_id;
                $object = $this->vendorRepository->update($attributes,$request->id);
            }
            else{
                $attributes['created_by'] = $user_id;

                $this->vendorRepository->create($attributes);
            }
            
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('vendor.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $department = $this->departmentRepository->all();
            $modeOfPayment = $this->paymentTypeRepository->all();
            $category = $this->categoryRepository->all();
            $data = $this->vendorRepository->find($id);
            $accountCategory = $this->accountCategoryRepository->allDesc();
            return view('admin.vendors.create',compact('data','department','modeOfPayment','category','accountCategory'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            // dd('delete');
            try {
                $object = $this->vendorRepository->find($id);
                if($object)
                {
                    $this->vendorRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Record is not deleted!',
                ]);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Data is Successfully Deleted!',
            ]);
        }
        else
        {
            return view('errors.401');
        }
    }

    public function subCode($id,$department_id)
    {
        $object = $this->subAccountCategoryRepository->find($id);
        $it_code = null;
        $code = null;
        if ($object!=null) 
        {
            $code = $object->code;

            $where = [
                'sub_code'  => $code,
                'department_id' => $department_id,
            ];
            // $vendors = $this->vendorRepository->findByColumnMultiple('sub_code',$code);
            $vendors = $this->vendorRepository->whereMultiple($where);

            $temp_arr = [];
            foreach ($vendors as $key => $value) 
            {
                $temp_arr[$value->id] = $value->it_code;
            }

            if(!empty($temp_arr))
            {
                $max_key = max(array_keys($temp_arr));
                $it_code = (int)$temp_arr[$max_key];
                $it_code += 1;
            }
            else{
                $it_code = $code.sprintf("%06d", 1);
            }
        }
        else{
            $code = $object->code;
            $it_code = $code.sprintf("%06d", 1);
        }

        $data['code'] = $code;
        $data['it_code'] = $it_code;

        return $data;
    }

    public function importView()
    {
        $department = $this->departmentRepository->all();

        return view('admin.vendors.import',compact('department'));
    }

    public function import(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '4095M');
        try {
            DB::beginTransaction();
            Excel::import(new ItemNameImport($request->department_id),request()->file('file'));
            DB::commit();
        } 
        catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error','File is not uploaded! '.$th->getMessage());
        }
        return redirect()->back()->with('success','File is successfully uploaded!');
    }

    public function ajaxCallForVendorPagination(Request $request)
    {
        // dd($request->all());
        $search = $request->query('search', array('value' => '', 'regex' => false));
        $draw = $request->query('draw', 0);
        $start = $request->query('start', 0);
        $length = $request->query('length', 25);
        $order = $request->query('order', array(1, 'asc'));        

        $filter = $search['value'];
        
        $sortColumns = array(
            0 => 'id',
            1 => 'name',
            2 => 'sub_code',
            3 => 'it_code',
            4 => 'cnic',
            5 => 'ntn',
            6 => 'tax_rate',
            7 => 'payment_type_id',
            8 => 'category_id',
            9 => 'cheque_name',
            10 => 'department_id',
        );

        $query = $this->vendorRepository->vendorsPagination();

        if (!empty($filter)) 
        {
            $query->where('name', 'like', '%'.$filter.'%')
                ->orWhere('sub_code', 'like', '%'.$filter.'%')
                ->orWhere('it_code', 'like', '%'.$filter.'%')
                ->orWhere('cnic', 'like', '%'.$filter.'%')
                ->orWhere('ntn', 'like', '%'.$filter.'%')
                ->orWhere('tax_rate', 'like', '%'.$filter.'%')
                ->orWhere('cheque_name', 'like', '%'.$filter.'%');

            $department_id  =  $this->departmentRepository->like('name',$filter);
            
            foreach ($department_id as $key => $value) 
            {
                $query->orwhere('department_id','=',$value->id);
            }
        }

        $recordsTotal = $query->count();

        $sortColumnName = $sortColumns[$order[0]['column']];

        $query->orderBy($sortColumnName, $order[0]['dir'])
                ->take($length)
                ->skip($start);

        $json = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsTotal,
            'data' => [],
        );

        $details = $query->get();

        $i = $start + 1;
        foreach ($details as $detail) 
        {
            $btn = '';
            if(Auth::user()->hasPermissionTo($this->permissionEdit))
            {
                $btn .= '<a class="btn btn-secondary btn-sm view-btn ml-1 mt-1" href="'.route('supplyChain.show',$detail->id).'" data-toggle="tooltip" data-placement="top" title="View">
                                <i class="fa fa-eye"></i>
                            </a>';
            }
            
            if(Auth::user()->hasPermissionTo($this->permissionDelete))
            {
                
                $btn .= '<button class="btn btn-danger btn-sm ml-1 btn-delete" data-id="'.$detail->id.'" data-toggle="tooltip" data-placement="top" title="Delete">
                            <i class="fa fa-trash"></i>
                        </button>';
            }
            $json['data'][] = [
                $i++,
                @$detail->name, 
                @$detail->sub_code, 
                @$detail->it_code,
                @$detail->cnic,
                @$detail->ntn,
                @$detail->tax_rate,
                @$detail->modeOfPayment->name,
                @$detail->category->name,
                @$detail->cheque_name,
                @$detail->department->name,
                @$btn,
            ];
        }
        return $json;
    }
}
