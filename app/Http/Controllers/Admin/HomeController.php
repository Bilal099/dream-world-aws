<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('admin.home.index');
    }

    public function ckeditorImageUpload(Request $request){
        
        $temp = $request->upload->store('public/ckeditor');
        $Url =  asset(str_replace("public/","storage/",$temp));
        return response()->json(['url'=>$Url]);
    }
}
