<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use File;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.module.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(),[
            'name'          => 'required',
        ],
        [
            'name.required'         => 'This field is required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            $name = $request->name;
            $first_capital_name = \ucwords($name);
            $camel_case = Str::camel($first_capital_name);
            $pascal_case = Str::replace(' ','',$first_capital_name);

            /**
                Creating Controller file with or withOut resource controller.
            **/
            if ($request->has('resource_controller_check')) 
            {
                $controller_with_resource = 'make:controller Admin/?Controller -r';
                $str = Str::replace('?',$pascal_case, $controller_with_resource);
                \Artisan::call($str);
            }
            elseif($request->has('controller_check'))
            {
                $controller = 'make:controller Admin/?Controller';
                $str = Str::replace('?',$pascal_case, $controller);
                \Artisan::call($str);
            }

            /**
                Creating Model file with or withOut migration.
            **/
            if ($request->has('model_migration_check')) 
            {
                $model_with_migration = 'make:model ? -m';
                $str = Str::replace('?',$pascal_case, $model_with_migration);
                \Artisan::call($str);
            }
            elseif($request->has('model_check'))
            {
                $model = 'make:model ?';
                $str = Str::replace('?',$pascal_case, $model);
                \Artisan::call($str);
            }

            /**
                Creating repository files in repository and eloquent folder.
            **/
            if ($request->has('repository_check')) 
            {
                $repo_interface_string = '../app/Repository/?RepositoryInterface.php';
                $repo_string = '../app/Repository/Eloquent/?Repository.php';

                $repo_interface = Str::replace('?',$pascal_case, $repo_interface_string);
                $repo = Str::replace('?',$pascal_case, $repo_string);

                if (!file_exists($repo_interface)) 
                {
                    touch($repo_interface, strtotime('-1 days'));
                    $content = '';
                    $content = Storage::disk('repo')->get('repo_interface_temp.txt');
                    $content = Str::replace('~',$pascal_case, $content);
                    File::put($repo_interface, $content, true);
                }
                
                if (!file_exists($repo)) 
                {
                    touch($repo, strtotime('-1 days'));
                    $content = '';
                    $content = Storage::disk('repo')->get('repo_temp.txt');
                    $content = Str::replace('~',$pascal_case, $content);
                    File::put($repo, $content, true);
                }

                /*
                    Repository binding in provider file
                */
                $provider_file_path = '../app/Providers/RepositoryServiceProvider.php';
                $data = file_get_contents($provider_file_path);

                $temp_use1 =  'use App\Repository\?RepositoryInterface;' ;
                $use1 = Str::replace('?',$pascal_case, $temp_use1);
                
                $temp_use2 =  'use App\Repository\Eloquent\?Repository;' ;
                $use2 = Str::replace('?',$pascal_case, $temp_use2);

                $temp_bind = '        $this->app->bind(?RepositoryInterface::class, ?Repository::class);';
 
                $bind = Str::replaceArray('?', [$pascal_case, $pascal_case], $temp_bind);
                $rows = explode("\n", $data);
                $rcount = count($rows); 
                for ($l=0; $l<$rcount; $l++)
                {
                    $rowss = $rows[$l];
                    if ($rowss == "namespace App\Providers;\r")
                    {
                        array_splice($rows, $l + 1, 0, $use1."\n");
                        array_splice($rows, $l + 2, 0, $use2."\n");
                    }
                    
                    if ($rowss == "    public function register()\r")
                    {
                        array_splice($rows, $l + 2, 0, $bind."\n");
                    }
                }
                file_put_contents($provider_file_path,$rows) or die("<br>oops");
                
            }
            
            /**
                Creating view files in (resource/view/admin) folder.
            **/
            if ($request->has('index_blade_check') || $request->has('create_blade_check') || $request->has('edit_blade_check') || $request->has('show_blade_check')) 
            {
                $path = '../resources/views/admin/?';
                $path = Str::replace('?',$camel_case, $path);
                if (!file_exists($path)) 
                {
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
            }

            if ($request->has('index_blade_check')) 
            {
                $index_blade_string = '../resources/views/admin/?/index.blade.php';
                $index_blade = Str::replace('?',$camel_case, $index_blade_string);
                if (!file_exists($index_blade)) 
                {
                    touch($index_blade, strtotime('-1 days'));
                }
            }
            if ($request->has('create_blade_check')) 
            {
                $create_blade_string = '../resources/views/admin/?/create.blade.php';
                $create_blade = Str::replace('?',$camel_case, $create_blade_string);
                if (!file_exists($create_blade)) 
                {
                    touch($create_blade, strtotime('-1 days'));
                }
            }
            if ($request->has('edit_blade_check')) 
            {
                $edit_blade_string = '../resources/views/admin/?/edit.blade.php';
                $edit_blade = Str::replace('?',$camel_case, $edit_blade_string);
                if (!file_exists($edit_blade)) 
                {
                    touch($edit_blade, strtotime('-1 days'));
                }
            }
            if ($request->has('show_blade_check')) 
            {
                $show_blade_string = '../resources/views/admin/?/show.blade.php';
                $show_blade = Str::replace('?',$camel_case, $show_blade_string);
                if (!file_exists($show_blade)) 
                {
                    touch($show_blade, strtotime('-1 days'));
                }
            }
        } 
        catch (\Throwable $th) 
        {
            return Redirect::back()->with('Error','There is something wrong!');
        }
        return Redirect::back()->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
