<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\PaymentTypeRepositoryInterface;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class PaymentTypeController extends Controller
{
    private $paymentTypeRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(PaymentTypeRepositoryInterface $paymentTypeRepository)
    {
        $this->paymentTypeRepository = $paymentTypeRepository;

        // for permissions
        $this->permissionView       = 'payment-type-view';
        $this->permissionCreate     = 'payment-type-create';
        $this->permissionEdit       = 'payment-type-edit';
        $this->permissionDelete     = 'payment-type-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            $data = $this->paymentTypeRepository->allDesc();
            return view('admin.paymentType.index',\compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            return view('admin.paymentType.create');
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' =>  'required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $attributes = [
                    'name'          => $request->name,
                    'updated_by'    => $user_id,
                ];
                $object = $this->paymentTypeRepository->update($attributes,$request->id);
            }
            else{
                $attributes = [
                    'name'          => $request->name,
                    'created_by'    => $user_id,
                ];
                $this->paymentTypeRepository->create($attributes);
            }
            
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('paymentType.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $data = $this->paymentTypeRepository->find($id);
            return view('admin.paymentType.create',compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            // dd('delete');
            try {
                $object = $this->paymentTypeRepository->find($id);
                if($object)
                {
                    $this->paymentTypeRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Record is not deleted!',
                ]);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Data is Successfully Deleted!',
            ]);
        }
        else
        {
            return view('errors.401');
        }
    }
}
