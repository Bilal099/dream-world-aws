<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\DepartmentBankRepositoryInterface;
use App\Repository\VendorRepositoryInterface;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class DepartmentBankController extends Controller
{
    private $departmentBankRepository;
    private $vendorRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(
        DepartmentBankRepositoryInterface $departmentBankRepository,
        VendorRepositoryInterface $vendorRepository
    )
    {
        $this->departmentBankRepository = $departmentBankRepository;
        $this->vendorRepository     = $vendorRepository;

        // for permissions
        $this->permissionView       = 'department-bank-view';
        $this->permissionCreate     = 'department-bank-create';
        $this->permissionEdit       = 'department-bank-edit';
        $this->permissionDelete     = 'department-bank-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = [];
        $data = $this->departmentBankRepository->allDesc();

        return view('admin.departmentBank.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            return view('admin.departmentBank.create');
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(),[
            'vendor_id' =>  'required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        // default
        // vendor_id
        $attributes['vendor_id'] = $request->vendor_id;
        $attributes['default'] = $request->has('default')? 1:0;

        try {
            
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $default_record = $this->departmentBankRepository->findByColumnSingle('default',1);
                // dd($default_record);
                if($default_record != null)
                {
                    $remove_default = $this->departmentBankRepository->update(['default'=>0],$default_record->id);
                }
                $attributes['updated_by'] = $user_id;
                $object = $this->departmentBankRepository->update($attributes,$request->id);
            }
            else{
                $attributes['created_by'] = $user_id;
                $this->departmentBankRepository->create($attributes);
            }
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('departmentBank.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $data = $this->departmentBankRepository->find($id);
            return view('admin.departmentBank.create',compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            try {
                $object = $this->departmentBankRepository->find($id);
                if($object)
                {
                    $this->departmentBankRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return redirect()->back()->with('error','Some thing is wrong!');
            }
            return redirect()->back()->with('success','Data is Successfully Deleted!');
        }
        else
        {
            return view('errors.401');
        }
    }
}
