<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\DepartmentRepositoryInterface;
use App\Repository\VendorRepositoryInterface;
use App\Repository\PaymentTypeRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\SubAccountCategoryRepositoryInterface;
use Illuminate\Support\Facades\Http;

use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class AjaxController extends Controller
{
    private $departmentRepository;
    private $vendorRepository;
    private $paymentTypeRepository;
    private $categoryRepository;
    private $subAccountCategoryRepository;

    
    public function __construct(
        VendorRepositoryInterface $vendorRepository, 
        DepartmentRepositoryInterface $departmentRepository,
        PaymentTypeRepositoryInterface $paymentTypeRepository,
        CategoryRepositoryInterface $categoryRepository,
        SubAccountCategoryRepositoryInterface $subAccountCategoryRepository
    )
    {
        $this->departmentRepository     = $departmentRepository;
        $this->vendorRepository         = $vendorRepository;
        $this->paymentTypeRepository    = $paymentTypeRepository;
        $this->categoryRepository       = $categoryRepository;
        $this->subAccountCategoryRepository = $subAccountCategoryRepository;
    }

    public function getVendorByDepartment(Request $request)
    {
        try {
            // $data = $this->vendorRepository->findByColumnMultiple('department_id',$request->department_id);
            $data = $this->vendorRepository->where([['department_id','=',$request->department_id],['name', 'like', '%'.$request->value.'%']]);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>true,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }

    public function getVendorDetail(Request $request)
    {
        // dd($request->all());
        $attributes = [];
        $attributes = [
            "name"          => $request->item_name,
            "cheque_name"   => $request->item_name,
            "sub_code"      => $request->sub_code,
            "it_code"       => $request->it_code,
            "ntn"           => $request->ntno,
            "cnic"          => $request->cnicno,
            "tax_rate"      => $request->taxrate,
            "department_id" => $request->department_id,
        ];
        
        try {
            // $data = $this->vendorRepository->find($request->id);
            $data = $this->vendorRepository->whereSingle([
                ["sub_code",'=',$request->sub_code],
               [ "it_code",'=',$request->it_code]
            ]);
            if ($data != null) 
            {
                // dd($data);
                $data->name      = $attributes['name'];
                $data->cheque_name      = $attributes['name'];
                $data->sub_code  = $attributes['sub_code'];
                $data->it_code   = $attributes['it_code'];
                $data->ntn       = $attributes['ntn'];
                $data->cnic      = $attributes['cnic'];
                $data->tax_rate  = $attributes['tax_rate'];
                $data->save();
            } 
            else {
                $data = $this->vendorRepository->create($attributes);
            }
            
            // dd("abc",$data);
            // "item_name" => "PARAMOUNT TESTING LABOURTORY"
            // "sub_code" => "3101",
            // "it_code" => "3101000066",
            // "ntno" => null,
            // "cnicno" => null,
            // "taxrate" => "0",
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>false,
                'msg' => 'There is some issue. '.$th->getMessage(),
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }

    public function getSubCategoryByDepartment(Request $request)
    {
        try {
            $data = $this->subAccountCategoryRepository->findByColumnMultiple('department_id',$request->department_id);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>false,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }
    
    public function getVendorBySubCategory(Request $request)
    {
        try {
            $data = $this->vendorRepository->findByColumnMultiple('sub_category_id',$request->sub_category_id);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>false,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }

    public function getVendorByName(Request $request)
    {
        // dd($request->all());
        try {
            $data = $this->vendorRepository->where([['name', 'like', '%'.$request->value.'%']]);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>true,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }

    public function getItemDetailFromLocal(Request $request)
    {
        /**
         * Get data from dream world data.
         */
        // dd($request->all());

        
        // dd($response,$response->json());
        try {
            $url = "http://118.103.236.10:8080/Accountapi/api/VCH/getfeedbackdropdowntoall?itname=".$request->value."&database=".$request->database;
            $response = Http::get($url);
            $request_response = $response->json();
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>true,
                'msg' => $th->getMessage(),
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $request_response,
        ]);
        
    }
}
