<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\SubAccountCategoryRepositoryInterface;
use App\Repository\CurAccountCategoryRepositoryInterface;
use App\Repository\DepartmentRepositoryInterface;

use App\Imports\SubAccountCategoriesImport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class SubAccountCategoryController extends Controller
{
    private $subAccountCategoryRepository;
    private $curAccountCategoryRepository;
    private $departmentRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(SubAccountCategoryRepositoryInterface $subAccountCategoryRepository, DepartmentRepositoryInterface $departmentRepository, CurAccountCategoryRepositoryInterface $curAccountCategoryRepository)
    {
        $this->subAccountCategoryRepository = $subAccountCategoryRepository;
        $this->curAccountCategoryRepository = $curAccountCategoryRepository;
        $this->departmentRepository = $departmentRepository;
    
        // for permissions
        $this->permissionView       = 'cur-accountant-category-view';
        $this->permissionCreate     = 'cur-accountant-category-create';
        $this->permissionEdit       = 'cur-accountant-category-edit';
        $this->permissionDelete     = 'cur-accountant-category-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            $data = $this->subAccountCategoryRepository->allDesc();
            return view('admin.subAccountCategories.index',\compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            $department = $this->departmentRepository->all();
            return view('admin.subAccountCategories.create',compact('department'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'              =>  'required',
            'code'              => 'required',
            'department_id'     =>  'required',
            'cur_id' => 'required',
        ],[
            'department_id.required'      => 'This fields is required',
            'cur_id.required'      => 'This fields is required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            
            $attributes = [];
            DB::beginTransaction();
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $attributes = [
                    'name'                  => $request->name,
                    'code'                  => $request->code,
                    'cur_id'                => $request->cur_id,
                    'department_id'         => ($request->department_id!=null)? $request->department_id:null,
                    'updated_by'            => $user_id,
                ];
                $object = $this->subAccountCategoryRepository->update($attributes,$request->id);
            }
            else{
                $attributes = [
                    'name'                  => $request->name,
                    'code'                  => $request->code,
                    'cur_id'                => $request->cur_id,
                    'department_id'         => ($request->department_id!=null)? $request->department_id:null,
                    'created_by'            => $user_id,
                ];
                $this->subAccountCategoryRepository->create($attributes);
            }
            DB::commit();
        } 
        catch (\Throwable $th) 
        {
            DB::rollback();
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('subAccountCategory.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $data = $this->subAccountCategoryRepository->find($id);
            $main = $this->curAccountCategoryRepository->findByColumnMultiple('department_id',$data->department_id);
            $department = $this->departmentRepository->all();
            return view('admin.subAccountCategories.create',compact('data','department','main'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            try {
                $object = $this->subAccountCategoryRepository->find($id);
                if($object)
                {
                    $this->subAccountCategoryRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return redirect()->back()->with('error','Some thing is wrong!');
            }
            return redirect()->back()->with('success','Data is Successfully Deleted!');
        }
        else
        {
            return view('errors.401');
        }
    }

    public function getCurCategoryByDepartment(Request $request)
    {
        try {
            $data = $this->curAccountCategoryRepository->findByColumnMultiple('department_id',$request->department_id);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>false,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }

    public function importView()
    {
        $department = $this->departmentRepository->all();

        return view('admin.subAccountCategories.import',compact('department'));
    }
    public function import(Request $request)
    {
        try {
            DB::beginTransaction();
            Excel::import(new SubAccountCategoriesImport($request->department_id),request()->file('file'));
            DB::commit();
        } 
        catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error','File is not uploaded!');
        }
        return redirect()->back()->with('success','File is successfully uploaded!');
    }
}
