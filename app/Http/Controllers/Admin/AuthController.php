<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repository\AdminRepositoryInterface;
use Validator;
use Hash;
use App\Services\GoogleRecaptchaService;
use App\Rules\Recaptcha;
class AuthController extends Controller
{
    private $adminRepository;

    // public function __construct()
    // {
    //     // $this->middleware('guest:admin')->except('logout');
    // }
    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }
    public function loginShow()
    {
        return view('admin.auth.login');
    }
    public function loginPost(Request $request,GoogleRecaptchaService $service)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
            'g-recaptcha-response' => [new Recaptcha]
        ]);
        if (Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password,'active'=>1])) 
        {
            $request->session()->regenerate();
            return redirect()->intended('admin');
        }
        return back()->withInput($request->only('email', 'remember'))->withMessage('Invalid username or password');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'oldpassword' => 'required',
            'newpassword' => 'required|confirmed|min:6',
        ]);
        if ($validator->passes()) {
            if(Hash::check($request->oldpassword, Auth::user()->password))
            {
                $this->adminRepository->changePassword(Auth::user()->id,Hash::make($request->newpassword));
                return response()->json(['status'=>true]);
            }
            else
            {
                $validator->getMessageBag()->add('password', 'Old Password doesn\'t matched');
                return response()->json(['status'=>false,'error'=>$validator->errors()->all()]);
            }
        }
        else
        {
            return response()->json(['status'=>false,'error'=>$validator->errors()->all()]);
        }
    }

    // public function ViewRegister()
    // {
    //     return view('admin.auth.register');
    // }

    // public function register(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'full_name'           => 'required',
    //         'email'               => 'required|unique:admins,email',
    //         'password'            => 'required|min:8',
    //         'confirm_password'    => 'required|min:8|same:password',
    //     ]);
    //     $validated = $validator->validated();
    //     $attributes = array(
    //         'name'      => $request->full_name,
    //         'email'     => $request->email,
    //         'password'  => Hash::make($request->password),
    //     );

    //     if ($validator->passes()) 
    //     {
    //         $this->adminRepository->register($attributes);
    //         return redirect()->route('admin.login')->with(['status'=>true]);
    //     }
    //     else
    //     {
    //         return redirect()->back()->with(['status'=>false,'error'=>$validator->errors()->all()]);
    //     }
    // }
}
