<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\SupplyChainRepositoryInterface;
use App\Repository\VendorRepositoryInterface;
use App\Repository\PaymentTypeRepositoryInterface;
use App\Repository\DirectorRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\PaymentRepositoryInterface;
use App\Repository\PaymentLogRepositoryInterface;
use App\Repository\VoucharRepositoryInterface;
use App\Repository\VoucharDetailRepositoryInterface;

use Auth;
use DB;
use Validator;
use Session;
use Redirect;
use PDF;

class AccountantController extends Controller
{
    private $supplyChainRepository;
    private $vendorRepository;
    private $paymentTypeRepository;
    private $directorRepository;
    private $categoryRepository;
    private $paymentRepository;
    private $paymentLogRepository;
    private $voucharRepository;
    private $voucharDetailRepository;



    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(
        PaymentLogRepositoryInterface $paymentLogRepository, 
        VendorRepositoryInterface $vendorRepository, 
        PaymentRepositoryInterface $paymentRepository, 
        SupplyChainRepositoryInterface $supplyChainRepository,
        PaymentTypeRepositoryInterface $paymentTypeRepository,
        DirectorRepositoryInterface $directorRepository,
        CategoryRepositoryInterface $categoryRepository,
        VoucharRepositoryInterface $voucharRepository,
        VoucharDetailRepositoryInterface $voucharDetailRepository
    )
    {
        $this->supplyChainRepository    = $supplyChainRepository;
        $this->vendorRepository         = $vendorRepository;
        $this->paymentTypeRepository    = $paymentTypeRepository;
        $this->directorRepository       = $directorRepository;
        $this->categoryRepository       = $categoryRepository;
        $this->paymentRepository        = $paymentRepository;
        $this->paymentLogRepository     = $paymentLogRepository;
        $this->voucharRepository        = $voucharRepository;
        $this->voucharDetailRepository  = $voucharDetailRepository;

        // for permissions
        $this->permissionView       = 'accountant-view';
        $this->permissionCreate     = 'accountant-create';
        $this->permissionEdit       = 'accountant-edit';
        $this->permissionDelete     = 'accountant-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
           $vouchars = $this->voucharRepository->allDesc();
            // $vendors  = $this->vendorRepository->vendorsInSupplyChain();
            $vendors  = $this->vendorRepository->vendorsInPayment();
            // $data = $this->supplyChainRepository->allDesc();
            // $data = $this->supplyChainRepository->getAllUnPaid();
            // $temp_array = [];
            // foreach ($data as $key => $value) 
            // {
            //     $temp_array[] = $value->id;
            // }
            // $in_process_payment = $this->paymentRepository->inProcess($temp_array);
            // $id_check1 = [];
            // foreach ($in_process_payment as $key => $value) 
            // {
            //     $id_check1[] = $value->supply_chain_id;
            // }
            // $in_process = array_unique($id_check1);
            // return view('admin.accountant.index',\compact('data','in_process','vendors'));
            return view('admin.accountant.index',\compact('vendors','vouchars'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $temp_array = array();
            $temp_array[] = $id; // convert int into array, because the whereIn method parameter get string and array
            $data = $this->supplyChainRepository->whereIn('id',$temp_array);
            $payment_type = $this->paymentTypeRepository->all();
            $director = $this->directorRepository->all();
            $category = $this->categoryRepository->all();
            return view('admin.accountant.edit',compact('data','payment_type','director','category'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bulkApproveByAccountant($json_request)
    {
        if(Auth::user()->hasPermissionTo('supply-chain-daily-sheet'))
        {
            $decode = base64_decode($json_request);
            $ids = json_decode($decode);
            // $data = $this->supplyChainRepository->whereIn('id',$ids);
            $data = $this->vendorRepository->whereIn('id',$ids);
            $payment_type = $this->paymentTypeRepository->all();
            $director = $this->directorRepository->all();
            return view('admin.accountant.approve',compact('data','payment_type','director'));
            
        }
        else
        {
            return view('errors.401');
        }
    }
    public function storeApprovedByAccountant(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(),[
            // 'remaining_amount.*'         =>  'required',
            'approved_amount.*'          =>  'required|numeric',
            // 'approved_by.*'              =>  'required',
            
        ],
        [
            // 'mode_of_payment.*.required'             => 'This field is required',
            'approved_amount.*.required'         => 'This field is required',
            // 'approved_by.*.required'             => 'This field is required',
            
        ]);

        if($validator->fails())
        {
            // dd(Redirect::back()->withInput($request->input())->withErrors($validator));
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }
        // dd($request->all());

        try {
            DB::beginTransaction();
            $user_id = Auth::user()->id;
            foreach ($request->id as $key => $value) 
            {
                // $remaining_amount =  $request->remaining_amount[$value] - $request->approved_amount[$value];
                // $attributes = [
                //     'remaining_amount'                  => $remaining_amount,
                //     'status'                            => ($remaining_amount == 0)? 2:0,
                //     // 'approved_amount_by_accountant'     => $request->approved_amount[$value],
                //     // 'accountant_id'                     => $user_id,
                // ];
                // $this->supplyChainRepository->update($attributes,$value);

                $attributes = [
                    'amount_paid'   => $request->approved_amount[$value],
                    'state'             => 1, // 1 means approve by accountant
                    'status'            => 1, // 0 means status is in process
                    'updated_by'        => $user_id,
                ];

                // $temp_obj = $this->paymentRepository->inProcessLatest($request->id);
                // $temp_obj = $this->paymentRepository->inProcessLatest($value);

                // dd($temp_obj);

                $object = $this->paymentRepository->update($attributes,$request->payment_id[$value]);
                // dd($object,'bilal');
                $log = [
                    'payment_id'    => $object,
                    'review_date'   => now(),
                    'reviewer'      => $user_id,
                    'status'        => 1,
                    'reviewer_role' => 'accountant',
                ];
                // $this->paymentLogRepository->create($log);

                $payment = $this->paymentRepository->find($request->payment_id[$value]);
                $department_id = $payment->vendor->department_id;
                // dd($payment->vendor->tax_rate);
                $vouchar_attributes = [];
                $vouchar_attributes = [
                    'payment_id'    => $request->payment_id[$value],
                    'vouchar_date' => now(),
                    'VCH'          => 'DV',
                    // 'EN_No'        => '',
                    // 'narration'    => '',
                    'total_credit' => $request->approved_amount[$value],
                    // 'total_debit'  => '',
                    'created_by'   => $user_id,
                    'department_id'=> $department_id,
                ];
                $this->voucharRepository->create($vouchar_attributes);

                // $this->supplyChainRepository->update($attributes,$value);
            }
            DB::commit();
        } 
        catch (\Throwable $th) 
        {
            DB::rollback();
            return redirect()->back()->with('error','Some thing is wrong!'.$th->getMessage());
        }
        return redirect()->route('accountant.index')->with('success','Data is Successfully Added');
    }

    public function bulkUpdate(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'date.*'                    => 'required',
            'chq_name.*'                => 'required',
            'cnic.*'                    => 'required',
            'gt_item_code.*'            => 'required',
            'nature_of_work.*'          => 'required',
            'purchaser_person.*'        => 'required',
            'total_amount.*'            => 'required',
            'category.*'                => 'required',
            'mode_of_payment.*'         => 'required',
            'approved_amount.*'         => 'required',
            'approved_by.*'             => 'required',
            'approved_amount_account.*' => 'required',
        ],
        [
            'date.*.required'                       => 'This field is required',
            'chq_name.*.required'                   => 'This field is required',
            'cnic.*.required'                       => 'This field is required',
            'gt_item_code.*.required'               => 'This field is required',
            'nature_of_work.*.required'             => 'This field is required',
            'purchaser_person.*.required'           => 'This field is required',
            'total_amount.*.required'               => 'This field is required',
            'category.*.required'                   => 'This field is required',
            'mode_of_payment.*.required'            => 'This field is required',
            'approved_amount.*.required'            => 'This field is required',
            'approved_by.*.required'                => 'This field is required',
            'approved_amount_account.*.required'    => 'This field is required',
        ]);

        if($validator->fails())
        {
            // dd(Redirect::back()->withInput($request->input())->withErrors($validator));
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }
        // dd($request->all());

        // try {
            $user_id = Auth::user()->id;
            foreach ($request->id as $key => $value) 
            {
                $attributes = [
                    'date'                          => $request->date[$value],
                    'cheque_name'                   => $request->chq_name[$value],
                    'cnic_number'                   => $request->cnic[$value],
                    'item_code'                     => $request->gt_item_code[$value],
                    'work_of_nature'                => $request->nature_of_work[$value],
                    'purchaser_person'              => $request->purchaser_person[$value],
                    'total_amount'                  => $request->total_amount[$value],
                    'category_id'                   => $request->category[$value],
                    'payment_type_id'               => $request->mode_of_payment[$value],
                    'approved_amount_by_director'   => $request->approved_amount[$value],
                    'director_id'                   => $request->approved_by[$value],
                    'approved_amount_by_accountant' => $request->approved_amount_account[$value],
                    'updated_by'                    => $user_id,
                ];
                $this->supplyChainRepository->update($attributes,$value);
            }
        // } 
        // catch (\Throwable $th) 
        // {
        //     return redirect()->back()->with('error','Some thing is wrong!');
        // }
        return redirect()->route('accountant.index')->with('success','Data is Successfully Added');
    }
}
