<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\Mail;
use App\Repository\AdminRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Auth;
use Hash;
use Redirect;
use Validator;
use Session;

use App\Services\GoogleRecaptchaService;
use App\Rules\Recaptcha;

class ForgetPasswordResetController extends Controller
{
    private $adminRepository, $userRepository;
    
    public function __construct(AdminRepositoryInterface $adminRepository,UserRepositoryInterface $userRepository)
    {
        $this->adminRepository  = $adminRepository;
        $this->userRepository   = $userRepository;
    }

    public function viewForgotpassword($type)
    {
        if($type == "admin")
        {
            return view('admin.auth.forgotPassword');
        }
        elseif($type == "user")
        {
            return view('website.auth.forgotPassword');
        }
    }

    public function AjaxCallForForgotPasswordEmail(Request $request,GoogleRecaptchaService $service)
    {
        $detail = array();

        $validator = Validator::make($request->all(),[
            'email' => 'required',
            'g_recaptcha_response' => new Recaptcha
        ]);

        if($validator->fails())
        {
            // dd($validator);
            $detail[0] = false;
            $detail[1] = "Please click on check box!";
            return $detail;
        }

        $to_email       = $request->email;
        $main_type      = $request->main_type;
        // $email_encode   = $to_email."-".date_format(NOW(),'Ymd')."-".$main_type;
        $email_encode   = $to_email."-".date('Y/m/d H:i:s')."-".$main_type;

        for ($i=0; $i < 3; $i++) { 
            $email_encode = base64_encode($email_encode);
        }
        

        if($main_type == "admin")
        {
            $admin  = $this->adminRepository->findByColumnSingle('email',$to_email);
            if($admin != null)
            {
                Mail::to($to_email)->send(new ForgotPassword($email_encode,$main_type));
                $detail[0] = true;
                $detail[1] = "Email sent successfully!";
                return $detail;
            }
            else{
                $detail[0] = false;
                $detail[1] = "Invalid Email Address!";
                return $detail;
            }
        }
        elseif($main_type == "user")
        {
            $User  = $this->userRepository->findByColumnSingle('email',$to_email);

            if($User != null)
            {
                Mail::to($to_email)->send(new ForgotPassword($email_encode,$main_type));
                $detail[0] = true;
                $detail[1] = "Email is Succesfully send!";
                return $detail;
            }
            else{
                $detail[0] = false;
                $detail[1] = "Invalid Email!";
                return $detail;
            }
        }
        
    }

    public function ResetPassword($email_encode)
    {
        $email_decode = $email_encode;
        for ($i=0; $i < 3; $i++) { 
            $email_decode = base64_decode($email_decode);
        }
        $email_decode   = explode('-',$email_decode);
        $email          = $email_decode[0];
        $expire_time    = $email_decode[1];
        $expire_time    = str_replace('/','-',$expire_time);
        $main_type      = $email_decode[2];
        session()->put('email',$email);
        // dd(date_format(date_create($expire_time), 'Y-m-d'));
        // dd(date('Y-m-d'));
        if($main_type == "admin")
        {
            $object  = $this->adminRepository->findByColumnSingle('email',$email);
            if((date_format(date_create($expire_time), 'Y-m-d') == date('Y-m-d')) && ($object->updated_at < $expire_time))
            {
                return view('admin.auth.resetPassword',compact('email'));
            }
            else{
                return view('errors.404');
            }
        }
        elseif($main_type == "user")
        {
            $object  = $this->userRepository->findByColumnSingle('email',$email);
            if((date_format(date_create($expire_time), 'Y-m-d') == date('Y-m-d H:i:s')) && ($object->updated_at < $expire_time))
            {
                return view('website.auth.resetPassword',compact('email'));
            }
            else{
                return view('errors.404');
            }
        }
    }

    public function SetNewPassword(Request $request)
    {
        $request->validate([
            'password'              => 'min:8|required_with:confirm_password|same:confirm_password',
            'confirm_password'      => 'min:8',
        ],[
            'password.required'              => 'This field is required',
            'confirm_password.required'      => 'This field is required',
        ]);
        if(session()->has('email')){
            if(session()->get('email') != $request->email){
                return view('errors.404');
            }
        }
        try {

            if($request->password_type == "admin")
            {
                $admin  = $this->adminRepository->findByColumnSingle('email',$request->email);
                if($admin != null)
                {
                    $object = $this->adminRepository->changePassword($admin->id,Hash::make($request->password));
                }
            }
            elseif($request->password_type == "user")
            {
                $User  = $this->userRepository->findByColumnSingle('email',$request->email);
                if($User != null)
                {
                    $object = $this->userRepository->changePassword($User->id,Hash::make($request->password));
                }
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('message',$th);
        }

        if($request->password_type == "admin")
        {
            return Redirect::to('/admin/login')->with('success','Your password reset successfully!');   
        }
        elseif($request->password_type == "user")
        {
            return Redirect::to('/login')->with('success','Your password reset successfully!');
        }
    }
}
