<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\CurAccountCategory;
use App\Models\SubAccountCategory;
use App\Models\Vendor;
use Auth;

class ItemNameImport implements ToCollection
{
    private $department_id;
    public function __construct($department_id)
    {
        $this->department_id = $department_id;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $userId = Auth::user()->id;
        $i = 0;
        $cat = SubAccountCategory::where([['department_id','=',$this->department_id]])->get();
        $category = [];
        foreach ($cat as $key => $value) 
        {
            $category[$value->code] = $value->id;
        }
        // dd(@$category[1323]);
        // dd($cat->where('code',6)->first());

        foreach ($collection as $row) 
        {
            if ($row[0] != 'SUB_CODE') 
            {
                // if(!Vendor::where([ ['name','=',$row[2]],['department_id','=',$this->department_id],['it_code','=',$row[1]] ])->exists())
                {
                    // $cat = SubAccountCategory::where([['department_id','=',$this->department_id],['code','=', $row[0]]])->first();
                    // dd($cat,$row);
                    // if($category[@$row[0]] != null)
                    {
                        $object = Vendor::create([
                            'sub_code'          => @$row[0],      
                            'sub_category_id'   => @$category[@$row[0]],
                            'it_code'           => @$row[1],
                            'name'              => @$row[2],
                            'cheque_name'       => @$row[2],
                            'ntn'               => @$row[4],
                            'cnic'              => @$row[5],
                            'tax_rate'          => @$row[7],
                            'department_id'     => @$this->department_id,
                            'created_by'        => @$userId,
                        ]);
                    }
                    
                }
            }
        }
    }
}
