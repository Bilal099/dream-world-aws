<?php

namespace App\Imports;

use App\Models\CurAccountCategory;
use App\Models\AccountCategory;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Auth;

class CurAccountCategoriesImport implements ToCollection
{
    private $department_id;
    public function __construct($department_id)
    {
        $this->department_id = $department_id;
    }
    
    public function collection(Collection $rows)
    {
        $userId = Auth::user()->id;
        $i = 0;
        foreach ($rows as $row) 
        {
            if ($row[0] != 'M_CODE') 
            {
                // if(!CurAccountCategory::where([ ['name','=',$row[2]],['department_id','=',$this->department_id],['code','=',$row[1]] ])->exists())
                {
                    $cat = AccountCategory::where([['department_id','=',$this->department_id],['code','=', $row[0]]])->first();
                    // dd($cat,$row);
                    if($cat != null)
                    {
                        $object = CurAccountCategory::create([
                            'name'                  => $row[2],
                            'code'                  => $row[1],
                            'department_id'         => $this->department_id,
                            // 'account_category_id'   => $this->accountCategoryId,
                            'account_category_id'   => $cat->id,
                            'created_by'            => $userId,
                        ]);
                    }
                    
                }
            }
        }
    }
}
