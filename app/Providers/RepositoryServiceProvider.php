<?php

namespace App\Providers;

use App\Repository\EloquentRepositoryInterface; 
use App\Repository\AdminRepositoryInterface; 
use App\Repository\UserRepositoryInterface;
use App\Repository\SettingsRepositoryInterface;  
use App\Repository\SettingDetailRepositoryInterface;
use App\Repository\SupplyChainRepositoryInterface;
use App\Repository\DirectorRepositoryInterface;
use App\Repository\PaymentTypeRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\PaymentRepositoryInterface;
use App\Repository\PaymentLogRepositoryInterface;
use App\Repository\BankRepositoryInterface;
use App\Repository\BankAccountRepositoryInterface;
use App\Repository\DepartmentRepositoryInterface;
use App\Repository\VendorRepositoryInterface;
use App\Repository\AccountCategoryRepositoryInterface;
use App\Repository\CurAccountCategoryRepositoryInterface;
use App\Repository\SubAccountCategoryRepositoryInterface;
use App\Repository\VoucharRepositoryInterface;
use App\Repository\VoucharDetailRepositoryInterface;
use App\Repository\DepartmentBankRepositoryInterface;
use App\Repository\ChequeBankRepositoryInterface;


use App\Repository\Eloquent\AdminRepository;
use App\Repository\Eloquent\UserRepository; 
use App\Repository\Eloquent\BaseRepository; 
use App\Repository\Eloquent\SettingsRepository;  
use App\Repository\Eloquent\SettingDetailRepository; 
use App\Repository\Eloquent\SupplyChainRepository;
use App\Repository\Eloquent\DirectorRepository;
use App\Repository\Eloquent\PaymentTypeRepository;
use App\Repository\Eloquent\CategoryRepository;
use App\Repository\Eloquent\PaymentRepository;
use App\Repository\Eloquent\PaymentLogRepository;
use App\Repository\Eloquent\BankRepository;
use App\Repository\Eloquent\BankAccountRepository;
use App\Repository\Eloquent\DepartmentRepository;
use App\Repository\Eloquent\VendorRepository;
use App\Repository\Eloquent\AccountCategoryRepository;
use App\Repository\Eloquent\CurAccountCategoryRepository;
use App\Repository\Eloquent\SubAccountCategoryRepository;
use App\Repository\Eloquent\VoucharRepository;
use App\Repository\Eloquent\VoucharDetailRepository;
use App\Repository\Eloquent\DepartmentBankRepository;
use App\Repository\Eloquent\ChequeBankRepository;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {    
        $this->app->bind(EloquentRepositoryInterface::class,                        BaseRepository::class);
        $this->app->bind(AdminRepositoryInterface::class,                           AdminRepository::class);
        $this->app->bind(UserRepositoryInterface::class,                            UserRepository::class);
        $this->app->bind(SettingsRepositoryInterface::class,                        SettingsRepository::class);
        $this->app->bind(SettingDetailRepositoryInterface::class,                   SettingDetailRepository::class);
        $this->app->bind(SupplyChainRepositoryInterface::class,                     SupplyChainRepository::class);
        $this->app->bind(DirectorRepositoryInterface::class,                        DirectorRepository::class);
        $this->app->bind(PaymentTypeRepositoryInterface::class,                     PaymentTypeRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class,                        CategoryRepository::class);
        $this->app->bind(PaymentRepositoryInterface::class,                         PaymentRepository::class);
        $this->app->bind(PaymentLogRepositoryInterface::class,                      PaymentLogRepository::class);
        $this->app->bind(BankRepositoryInterface::class,                            BankRepository::class);
        $this->app->bind(BankAccountRepositoryInterface::class,                     BankAccountRepository::class);
        $this->app->bind(DepartmentRepositoryInterface::class,                      DepartmentRepository::class);
        $this->app->bind(VendorRepositoryInterface::class,                          VendorRepository::class);
        $this->app->bind(AccountCategoryRepositoryInterface::class,                 AccountCategoryRepository::class);
        $this->app->bind(CurAccountCategoryRepositoryInterface::class,              CurAccountCategoryRepository::class);
        $this->app->bind(SubAccountCategoryRepositoryInterface::class,              SubAccountCategoryRepository::class);
        $this->app->bind(VoucharRepositoryInterface::class,                         VoucharRepository::class);
        $this->app->bind(VoucharDetailRepositoryInterface::class,                   VoucharDetailRepository::class);
        $this->app->bind(DepartmentBankRepositoryInterface::class,                  DepartmentBankRepository::class);
        $this->app->bind(ChequeBankRepositoryInterface::class,                      ChequeBankRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
