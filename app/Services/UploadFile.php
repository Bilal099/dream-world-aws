<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class UploadFile
{
    public static function singleUpload($folderName, $image)
    {
        $imageName   = md5($folderName.mt_rand(1000000, 9999999).time()).'.'.$image->extension();
        Storage::disk('local')->put('public/'.$folderName.'/'.$imageName, File::get($image));
        return $imageName;
    }
    public static function multipleUpload($folderName, $album)
    {
        $Responce_container = array();
        foreach ($album as $index => $photo) {
            $photoName   = md5($folderName.mt_rand(1000000, 9999999).time().$index).'.'.$photo->extension();
            Storage::disk('local')->put('public/'.$folderName.'/'.$photoName, File::get($photo));
            $Responce_container[] = $photoName; 
        }
        return $Responce_container;
    }

    public static function base64ToImage($folderName, $base64)
    {
        $image_parts = explode(";base64,", $base64);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $photoName   = md5($folderName.mt_rand(1000000, 9999999).time()).'.png';
        $file = storage_path().'/app/public/'.$folderName.'/'. $photoName;
        Image::make(file_get_contents($base64))->save($file);
        return $photoName;
    }
}
