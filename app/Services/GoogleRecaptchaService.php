<?php
namespace App\Services;
use Illuminate\Support\Facades\Http;

class GoogleRecaptchaService
{
    public function validate($responseToken)
    {
        $response =  Http::asForm()->post('https://www.google.com/recaptcha/api/siteverify', [
                'secret' => config('services.recaptcha.secret'),
                'response' => $responseToken
        ]);
        $body = $response->json();
        // dd($body);
        return $body['success'];
    }
}
