<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface DirectorRepositoryInterface
{

    public function update(array $attributes,$id);
}