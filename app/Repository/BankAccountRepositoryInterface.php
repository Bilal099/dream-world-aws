<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface BankAccountRepositoryInterface
{

    public function update(array $attributes,$id);
}