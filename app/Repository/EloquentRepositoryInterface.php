<?php


namespace App\Repository;


use Illuminate\Database\Eloquent\Model;

/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface EloquentRepositoryInterface
{
   /**
    * @param array $attributes
    * @return Model
    */
   public function create(array $attributes): Model;

   /**
    * @param $id
    * @return Model
    */
   public function find($id): ?Model;


   public function findByColumnSingle($column,$value);

   public function findByColumnMultiple($column,$value);


   public function all();

   public function allActive();

   public function allDesc();

   public function whereIn($column,array $values);

   public function delete($id);

   public function whereMultiple(array $attributes);

   public function whereSingle(array $attributes);

   public function like($column,$value);

   public function where(array $attributes);

}