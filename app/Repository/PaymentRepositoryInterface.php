<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface PaymentRepositoryInterface
{
    public function update(array $attributes,$id);

    public function isComplete(array $values);

    public function inProcess(array $values);

   public function inProcessLatest($id);

}