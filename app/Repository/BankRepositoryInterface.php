<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface BankRepositoryInterface
{

    public function update(array $attributes,$id);
}