<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface VoucharRepositoryInterface
{
    public function update(array $attributes,$id);
}