<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface CategoryRepositoryInterface
{

    public function update(array $attributes,$id);
}