<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface SupplyChainRepositoryInterface
{
    public function update(array $attributes,$id);

    public function getAllUnPaid();

    public function getPaymentIsComplete();

    public function getPaymentInProcess();

    public function getPaymentnotInProcess();


}