<?php
namespace App\Repository\Eloquent;

use App\Models\PaymentType;
use App\Repository\PaymentTypeRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class PaymentTypeRepository extends BaseRepository implements PaymentTypeRepositoryInterface
{

   public function __construct(PaymentType $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}