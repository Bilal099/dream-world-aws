<?php
namespace App\Repository\Eloquent;

use App\Models\VoucharDetail;
use App\Repository\VoucharDetailRepositoryInterface;
use Illuminate\Support\Collection;

class VoucharDetailRepository extends BaseRepository implements VoucharDetailRepositoryInterface
{

    public function __construct(VoucharDetail $model)
    {
        parent::__construct($model);
    }

    public function update(array $attributes,$id)
    {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
    }
}