<?php
namespace App\Repository\Eloquent;

use App\Models\Payment;
use App\Repository\PaymentRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class PaymentRepository extends BaseRepository implements PaymentRepositoryInterface
{

   public function __construct(Payment $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

   public function isComplete(array $values)
   {
        return $this->model->where('status',1)->whereIn('supply_chain_id',$values)->get();
   }
   
   public function inProcess(array $values)
   {
        return $this->model->where('status',0)->whereIn('supply_chain_id',$values)->get();
   }

   public function inProcessLatest($id)
   {
        return $this->model->where('status',0)->where('supply_chain_id',$id)->first();
   }

   

}