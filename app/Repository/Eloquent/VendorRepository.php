<?php
namespace App\Repository\Eloquent;

use App\Models\Vendor;
use App\Repository\VendorRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\SupplyChain;
use App\Models\Payment;

class VendorRepository extends BaseRepository implements VendorRepositoryInterface
{

    public function __construct(Vendor $model)
    {
        parent::__construct($model);
    }

    public function update(array $attributes,$id)
    {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
    }

    public function vendorsInSupplyChain()
    {
        $supplyChain = SupplyChain::all()->pluck('vendor_id')->toArray();
        $vendor_ids = array_unique($supplyChain);
        $object = $this->model->whereIn('id',$vendor_ids)->get();
        return $object;
    }
    
    public function vendorsInSupplyChainForPagination()
    {
        $supplyChain = SupplyChain::all()->pluck('vendor_id')->toArray();
        $vendor_ids = array_unique($supplyChain);
        $object = $this->model->whereIn('id',$vendor_ids);
        return $object;
    }
    
    public function vendorsInPayment()
    {
        $payment = Payment::where('status',0)->pluck('vendor_id')->toArray();
        $vendor_ids = array_unique($payment);
        $object = $this->model->whereIn('id',$vendor_ids)->get();
        return $object;
    }

    public function vendorsPagination()
    {
        $object = $this->model->select('id', 'name','it_code', 'cnic', 'tax_rate', 'payment_type_id', 'category_id', 'cheque_name', 'department_id', 'account_category_id', 'sub_code', 'ntn');
        return $object;
    }

}