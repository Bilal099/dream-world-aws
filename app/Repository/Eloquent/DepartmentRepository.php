<?php
namespace App\Repository\Eloquent;

use App\Models\Department;
use App\Repository\DepartmentRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class DepartmentRepository extends BaseRepository implements DepartmentRepositoryInterface
{

   public function __construct(Department $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}