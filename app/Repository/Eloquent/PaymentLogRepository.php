<?php
namespace App\Repository\Eloquent;

use App\Models\PaymentLog;
use App\Repository\PaymentLogRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class PaymentLogRepository extends BaseRepository implements PaymentLogRepositoryInterface
{
   public function __construct(PaymentLog $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }
}