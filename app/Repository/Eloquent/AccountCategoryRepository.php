<?php
namespace App\Repository\Eloquent;

use App\Models\AccountCategory;
use App\Repository\AccountCategoryRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class AccountCategoryRepository extends BaseRepository implements AccountCategoryRepositoryInterface
{

   public function __construct(AccountCategory $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}