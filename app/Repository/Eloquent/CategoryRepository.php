<?php
namespace App\Repository\Eloquent;

use App\Models\Category;
use App\Repository\CategoryRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{

   public function __construct(Category $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}