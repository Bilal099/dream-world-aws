<?php
namespace App\Repository\Eloquent;

use App\Models\Bank;
use App\Repository\BankRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class BankRepository extends BaseRepository implements BankRepositoryInterface
{

   public function __construct(Bank $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}