<?php
namespace App\Repository\Eloquent;

use App\Models\Vouchar;
use App\Repository\VoucharRepositoryInterface;
use Illuminate\Support\Collection;

class VoucharRepository extends BaseRepository implements VoucharRepositoryInterface
{

    public function __construct(Vouchar $model)
    {
        parent::__construct($model);
    }

    public function update(array $attributes,$id)
    {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
    }
}