<?php
namespace App\Repository\Eloquent;

use App\Models\Policy;
use App\Models\PolicyTranslation;
use App\Repository\PolicyRepositoryInterface;
use Illuminate\Support\Collection;
use Auth;

class PolicyRepository extends BaseRepository implements PolicyRepositoryInterface
{
    public function __construct(Policy $model)
    {
       parent::__construct($model);
    }


   public function update(array $attributes,$id)
    {
        $objectTranslation;
        foreach ($attributes as $key => $value) 
        {
            $objectTranslation = PolicyTranslation::where([['locale','=',$key],['policy_id','=',$id]])->first();
            if($objectTranslation == null)
            {
                if (isset($value['content'])) 
                {
                    $objectTranslation                  = new PolicyTranslation;
                    $objectTranslation->locale          = $key;
                    $objectTranslation->content         = $value['content'];
                    $objectTranslation->policy_id       = $id;
                    $objectTranslation->save();
                }
            }
            else{
                $object                                 = $this->model->find($id);
                $object->translate($key)->content       = $value['content'];
                $object->updated_by                     = Auth::user()->id;
                $object->save();
            }
        }
    }

    public function delete($id)
    {
        $object = $this->model->find($id);
        $object->deleted_by = Auth::user()->id;
        $object->save();
        $object->delete();
    }

    public function createPolicy(array $attributes)
    {
        // dd($attributes);
        $object             = new $this->model;
        $object->active     = 1;
        $object->page_name  = $attributes['page_name'];
        $object->created_by = Auth::user()->id;
        $object->save();
        foreach ($attributes as $key => $value) 
        {
            if (isset($value['content'])) 
            {
                $objectTranslation                  = new PolicyTranslation;
                $objectTranslation->locale          = $key;
                $objectTranslation->content         = $value['content'];
                $objectTranslation->policy_id       = $object->id;
                $objectTranslation->save();
            }
            
        }
        return $object;
    }


}
