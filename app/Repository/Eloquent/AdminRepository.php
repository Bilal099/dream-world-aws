<?php
namespace App\Repository\Eloquent;

use App\Models\Admin;
use App\Repository\AdminRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class AdminRepository extends BaseRepository implements AdminRepositoryInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
   public function __construct(Admin $model)
   {
       parent::__construct($model);
   }

   /**
    * @return Collection
    */
   

   public function changePassword($id,$password)
   {
        $admin = $this->model->find($id);
        $admin->password = $password;
        $admin->save();
        return $admin;
   }

   public function register(array $attributes)
   {
       $admin               = new $this->model;
       $admin->name         = $attributes['name'];
       $admin->email        = $attributes['email']; 
       $admin->password     = $attributes['password'];
       $admin->save();
       return $admin;
   }
}