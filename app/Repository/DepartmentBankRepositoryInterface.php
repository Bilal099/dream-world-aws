<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface DepartmentBankRepositoryInterface
{
   public function update(array $attributes,$id);
}