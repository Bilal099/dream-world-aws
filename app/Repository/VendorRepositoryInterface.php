<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface VendorRepositoryInterface
{

    public function update(array $attributes,$id);
    public function vendorsInSupplyChain();
    public function vendorsInPayment();
    public function vendorsInSupplyChainForPagination();
    public function vendorsPagination();

}