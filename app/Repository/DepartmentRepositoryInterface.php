<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface DepartmentRepositoryInterface
{

    public function update(array $attributes,$id);
}