<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface VoucharDetailRepositoryInterface
{
    public function update(array $attributes,$id);
}