<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface SubAccountCategoryRepositoryInterface
{

    public function update(array $attributes,$id);
}