<?php

namespace Database\Factories;

use App\Models\Vendor;
use Illuminate\Database\Eloquent\Factories\Factory;

class VendorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vendor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->name;
        $department = array(1,2);
        $mop = array(1,2);
        $category = array(1,2,3,4,5,6,7);
        return [
            'name'              => $name,
            'cheque_name'       => $name,
            'it_code'           => $this->faker->numerify('####'),
            'cnic'          => $this->faker->numerify('#############'),
            'tax_rate'          => $this->faker->numerify('#'),
            'department_id'     => $department[array_rand($department)],
            'payment_type_id'   => $mop[array_rand($mop)],
            'category_id'       => $category[array_rand($category)],
        ];
    }
}
