<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplyChainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_chains', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->string('cheque_name')->nullable();
            $table->string('cnic_number')->nullable()->comment('NTN/CNIC Number');
            $table->string('item_code')->nullable();
            $table->string('work_of_nature')->nullable();
            $table->string('purchaser_person')->nullable();
            $table->bigInteger('total_amount')->nullable();
            $table->bigInteger('remaining_amount')->nullable();
            $table->tinyInteger('category_id')->nullable();
            $table->tinyInteger('payment_type_id')->nullable();
            $table->tinyInteger('director_id')->nullable();
            $table->bigInteger('approved_amount_by_director')->nullable();
            $table->tinyInteger('accountant_id')->nullable();
            $table->bigInteger('approved_amount_by_accountant')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0=pending, 1=in process, 2=complete');
            $table->tinyInteger('created_by')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->tinyInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_chains');
    }
}
