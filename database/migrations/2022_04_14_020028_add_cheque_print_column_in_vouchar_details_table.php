<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChequePrintColumnInVoucharDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vouchar_details', function (Blueprint $table) {
            $table->boolean('cheque_print_status')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vouchar_details', function (Blueprint $table) {
            $table->dropColumn('cheque_print_status');
        });
    }
}
