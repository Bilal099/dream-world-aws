<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100)->nullable()->comment('IT Name');
            $table->string('it_code', 100)->nullable()->comment('IT Code');
            $table->string('cnic', 100)->nullable();
            $table->string('ntn', 100)->nullable();
            $table->float('tax_rate')->nullable();
            $table->foreignId('payment_type_id')->nullable()->references('id')->on('payment_types')->comment('Mode of payment');
            $table->foreignId('category_id')->nullable()->references('id')->on('categories')->comment('Creditor Group');
            // $table->tinyInteger('creditor_tax_category')->nullable()->comment('0=services, 1=commisson, 2=supplier');
            $table->string('cheque_name', 100)->nullable();
            $table->foreignId('department_id')->references('id')->on('departments');
            $table->tinyInteger('created_by')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->tinyInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
