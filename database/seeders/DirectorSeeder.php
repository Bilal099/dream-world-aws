<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Director;

class DirectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            'Director 1',
            'Director 2',
            
        ];
        foreach ($category as $key => $value) 
        {
            if(!Director::where('name',$value)->exists())
            {
                Director::create(['name' => $value,'created_by' => 1]);
            }
        }
    }
}
