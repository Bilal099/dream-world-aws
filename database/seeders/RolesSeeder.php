<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate(['guard_name' => 'admin', 'name' => 'superadmin']);
        Role::firstOrCreate(['guard_name' => 'admin','name' => 'admin']);
        Role::firstOrCreate(['guard_name' => 'admin','name' => 'user']);
    }
}
