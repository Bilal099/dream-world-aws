<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            'Normal Limit',
            'Catering',
            'Facilities Maintaince',
            'Convention Center',
            'Deveploment',
            'Out Of Limt',
            'CAF',
        ];
        foreach ($category as $key => $value) 
        {
            if(!Category::where('name',$value)->exists())
            {
                Category::create(['name' => $value,'created_by' => 1]);
            }
        }
    }
}
