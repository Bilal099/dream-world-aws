<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Admin;
use Spatie\Permission\Models\Permission;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::firstOrCreate(
            [
                'name' => 'SuperAdmin',
                'email' => 'superadmin@admin.net'
            ],
            [
            'name' => 'SuperAdmin',
            'email' => 'superadmin@admin.net',
            'password' => Hash::make('12345678'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        if(!$admin->hasRole('superadmin')){
            $admin->assignRole('superadmin');
        }
        $permissions = Permission::select('name')->get();
        $admin->syncPermissions([]);
        foreach ($permissions as $key => $value) {
            $admin->givePermissionTo($value->name);
        }

        $subAdmin = Admin::firstOrCreate(
            [
                'name' => 'Admin',
                'email' => 'admin@admin.com'
            ],
            [
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('12345678'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        if(!$subAdmin->hasRole('admin')){
            $subAdmin->assignRole('admin');
        }
        $subAdmin->syncPermissions([]);
        foreach ($permissions as $key => $value) {
            if(!strpos($value, 'language') && !strpos($value, 'user')) 
            {
                $subAdmin->givePermissionTo($value->name);
            }
        }
    }
}
