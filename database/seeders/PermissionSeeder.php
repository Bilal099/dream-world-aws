<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'user-view',
            'user-create',
            'user-edit',
            'user-delete',
            
            'supply-chain-view',
            'supply-chain-create',
            'supply-chain-edit',
            'supply-chain-delete',
            'supply-chain-bulk-edit',
            'supply-chain-daily-sheet',

            'director-view',
            'director-create',
            'director-edit',
            'director-delete',

            'accountant-view',
            'accountant-create',
            'accountant-edit',
            'accountant-delete',

            'category-view',
            'category-create',
            'category-edit',
            'category-delete',

            'payment-type-view',
            'payment-type-create',
            'payment-type-edit',
            'payment-type-delete',

            'department-view',
            'department-create',
            'department-edit',
            'department-delete',

            'vendor-view',
            'vendor-create',
            'vendor-edit',
            'vendor-delete',
            
            'bank-view',
            'bank-create',
            'bank-edit',
            'bank-delete',
            
            'bank-account-view',
            'bank-account-create',
            'bank-account-edit',
            'bank-account-delete',

            'accountant-category-view',
            'accountant-category-create',
            'accountant-category-edit',
            'accountant-category-delete',

            'cur-accountant-category-view',
            'cur-accountant-category-create',
            'cur-accountant-category-edit',
            'cur-accountant-category-delete',
            
            'sub-accountant-category-view',
            'sub-accountant-category-create',
            'sub-accountant-category-edit',
            'sub-accountant-category-delete',

            'department-bank-view',
            'department-bank-create',
            'department-bank-edit',
            'department-bank-delete',
        ];
        foreach ($permissions as $key => $value) {
            if(!Permission::where('name',$value)->exists())
            {
                Permission::create(['guard_name' => 'admin','name' => $value]);
            }
        }
    }
}
