<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentType;


class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            'Cheque',
            'Pay Order',
        ];
        foreach ($category as $key => $value) 
        {
            if(!PaymentType::where('name',$value)->exists())
            {
                PaymentType::create(['name' => $value,'created_by' => 1]);
            }
        }
    }
}
