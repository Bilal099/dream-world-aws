<?php
namespace App\Repository\Eloquent;

use App\Models\~;
use App\Repository\~RepositoryInterface;
use Illuminate\Support\Collection;

class ~Repository extends BaseRepository implements ~RepositoryInterface
{
    public function __construct(~ $model)
    {
        parent::__construct($model);
    }

    public function update(array $attributes,$id)
    {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
    }
}